﻿CREATE TABLE [dbo].[Table]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [message] VARCHAR(50) NULL, 
    [ime] NCHAR(10) NOT NULL, 
    [prezime] NCHAR(10) NOT NULL
)
