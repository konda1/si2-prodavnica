﻿CREATE TABLE [dbo].[NabavniProizvod]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [tabela] VARCHAR(50) NOT NULL, 
    [id_u_tabeli] INT NOT NULL, 
    [broj_prodatih] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [AK_NabavniProizvod_tabela_idutabeli] UNIQUE ([tabela], [id_u_tabeli]), 
    CONSTRAINT [CK_NabavniProizvod_brojprodatih] CHECK ([broj_prodatih] >= 0) 
)

GO

CREATE TRIGGER [dbo].[After_NabavniProizvod_Insert]
	ON [dbo].[NabavniProizvod]
	FOR INSERT
	AS
	BEGIN
		SET NoCount ON
		INSERT INTO [dbo].[Porudzbina]
			([id_proizvoda], [kolicina])
		SELECT [Id], 0
		FROM inserted
	END

GO

--create trigger [dbo].[after_nabavniproizvod_update]
--    on [dbo].[nabavniproizvod]
--    for update
--    as
--    begin
--        set nocount on
--		-- determine loop boundaries.
--		declare @id int = 0
--		declare @counter int = 0
--		declare @total int = isnull((select count(1) from inserted), 0)
--		declare @nabavnacena real
--		declare @tabela varchar(50)
--		declare @id_u_tabeli int
--		declare @sqlcommand varchar(255)
    
--		-- iterate records.
--		while (@counter <> (@total))
--		begin  
--				-- record to proces.
--				set @id = (select id from inserted order by id offset @counter rows fetch next 1 rows only)
--				select @tabela = tabela, @id_u_tabeli = id_u_tabeli, @nabavnacena = nabavna_cena  from inserted where id = @id

--				-- exec sproc here:
--				set @sqlcommand = concat('update [dbo].[', @tabela, '] set [cena] = ', @nabavnacena, 'where [id] = ', @id_u_tabeli)
--				exec (@sqlcommand)
                    
--				-- increase counter to break the loop after all records are processed.
--				set @counter = @counter + 1
--		end
--    end

--go

CREATE TRIGGER [dbo].[After_NabavniProizvod_Delete]
    ON [dbo].[NabavniProizvod]
    FOR DELETE
    AS
    BEGIN
		-- Determine loop boundaries.
		declare @Id int = 0
		declare @counter int = 0
		declare @total int = isnull((select count(1) from deleted), 0)
		declare @Tabela varchar(50)
		declare @Id_u_tabeli int
		declare @SqlCommand varchar(255)
    
		-- Iterate records.
		while (@counter <> (@total))
		begin  
				-- record to proces.
				set @Id = (select Id from deleted order by Id offset @counter rows fetch next 1 rows only)
				select @Tabela = tabela, @Id_u_tabeli = id_u_tabeli from deleted where Id = @Id

				-- Exec sproc here:
				DECLARE @Query  NVARCHAR(1000) = CONCAT('SELECT * FROM [dbo].[', @Tabela, '] WHERE [id] = ', @Id_u_tabeli),
				@rowcnt INT

				EXEC (@Query)

				SELECT @rowcnt = @@ROWCOUNT

				IF @rowcnt > 0
				BEGIN
					set @SqlCommand = CONCAT('DELETE FROM [dbo].[', @Tabela, '] WHERE [id] = ', @Id_u_tabeli)
					EXEC (@SqlCommand)
				END 
                    
				-- Increase counter to break the loop after all records are processed.
				set @counter = @counter + 1
		end
    END

GO