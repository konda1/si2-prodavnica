﻿CREATE TABLE [dbo].[NeprodavaniProizvodi]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [tip_proizvoda] VARCHAR(50) NOT NULL, 
    [id_u_tabeli_proizvoda] INT NOT NULL,
	[sifra_proizvoda] INT NOT NULL,
    [stara_cena] REAL NOT NULL, 
    [trenutni_popust] INT NOT NULL,
	[aktivno] BIT NOT NULL,
	CONSTRAINT [popust_kod_neprodavanih_proizvoda] CHECK ([trenutni_popust] <= 15),
	CONSTRAINT [jedinstvena_sifra] UNIQUE([sifra_proizvoda])
)