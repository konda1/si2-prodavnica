﻿CREATE TABLE [dbo].[User]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [tip_korisnika] NCHAR(20) NOT NULL, 
    [username] VARCHAR(50) NOT NULL, 
    [password] VARCHAR(50) NOT NULL, 
    [ime] NCHAR(20) NULL, 
    [prezime] NCHAR(20) NULL
)
