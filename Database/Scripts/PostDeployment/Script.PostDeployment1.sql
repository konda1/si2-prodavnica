﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

-- DEFINICIJA KATEGORIJA
EXECUTE [dbo].[ResetujTabelu] @tabela = 'Kategorija'
GO
INSERT INTO [dbo].[Kategorija] ([naziv]) VALUES ('Adapter'), ('EksterniDisk'), ('HardDisk'), ('Kabl'), ('Mikrofon'), ('Mis'), ('Monitor'), ('Podloga'), ('Projektor'), ('Skener'), ('Slusalice'), ('Stampac'), ('Tastatura'), ('USBMemorija'), ('Zvucnik')

EXECUTE dbo.ResetujTabelu @tabela = 'User'
GO
INSERT INTO [dbo].[User] ([tip_korisnika], [username], [password], [ime], [prezime]) VALUES ('A', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'administrator', 'vlasnik')

EXECUTE [dbo].[ResetujTabelu] @tabela = 'NabavniProizvod'
GO
EXECUTE [dbo].[ResetujTabelu] @tabela = 'Porudzbina'
GO

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Dobavljac'
GO
INSERT INTO [dbo].[Dobavljac] ([naziv], [email]) VALUES (N'Alti', 'dobavljac@alti.rs'), (N'Emmi', 'contact@emmi.rs'), (N'Đole', 'djole77bg@gmail.com'), (N'технический поставщик', 'tehno@b.ru'), (N'műszaki szállító', 'szallito@site.hu')

EXECUTE [dbo].[ResetujTabelu] @tabela = 'KategorijeDobavljaca'
GO
INSERT INTO [dbo].[KategorijeDobavljaca] ([id_dobavljaca], [id_kategorije]) VALUES (1, 1), (1, 3), (1, 5), (1, 7), (1, 9), (1, 11), (1, 13), (1, 15)
INSERT INTO [dbo].[KategorijeDobavljaca] ([id_dobavljaca], [id_kategorije]) VALUES (2, 2), (2, 4), (2, 6), (2, 8), (2, 10), (2, 12), (2, 14), (2, 15)
INSERT INTO [dbo].[KategorijeDobavljaca] ([id_dobavljaca], [id_kategorije]) VALUES (3, 2), (3, 6), (3, 9), (3, 11), (3, 14), (3, 15)
INSERT INTO [dbo].[KategorijeDobavljaca] ([id_dobavljaca], [id_kategorije]) VALUES (4, 1), (4, 2), (4, 4), (4, 5), (4, 6), (4, 7), (4, 8), (4, 10), (4, 12), (4, 13), (4, 14)
INSERT INTO [dbo].[KategorijeDobavljaca] ([id_dobavljaca], [id_kategorije]) SELECT 5, [Id] FROM [dbo].[Kategorija]

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Adapter'
GO
INSERT INTO [dbo].[Adapter] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [snagaW], [izlazniNapon], [path], [datum_poslednje_kupovine]) VALUES ('MS', 'ENGINE 0161093', 6826442, 2490.0, 5, 2.0, 'www.google.com', 60, 19, 'pack://siteoforigin:,,,/img/adapterMS_Engine.jpg', '2018-12-12')
INSERT INTO [dbo].[Adapter] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [snagaW], [izlazniNapon], [path], [datum_poslednje_kupovine]) VALUES ('MS', 'COOL AC 90 SLIM', 6826444, 2990.0, 4, 2.0, 'www.google.com',  90, 19, 'pack://siteoforigin:,,,/img/adapterMS_Cool.jpg', GETDATE())
INSERT INTO [dbo].[Adapter] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [snagaW], [izlazniNapon], [path], [datum_poslednje_kupovine]) VALUES ('MS', 'ENGINE SLIM 0161094', 6826447, 3090.0, 3, 2.5, 'www.google.com', 60, 19,'pack://siteoforigin:,,,/img/adapterMS_EngineSLIM.jpg', GETDATE())
INSERT INTO [dbo].[Adapter] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [snagaW], [izlazniNapon], [path], [datum_poslednje_kupovine]) VALUES ('KDF', 'Multi 90', 6797860, 1890.0, 2, 1.0, 'www.google.com', 90, 19,'pack://siteoforigin:,,,/img/adapterKDF_Multi90.jpg', '2018-12-19')
INSERT INTO [dbo].[Adapter] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [snagaW], [izlazniNapon], [path], [datum_poslednje_kupovine]) VALUES ('KDF', 'Multi 65', 6797856, 1890.0, 1, 1.0, 'www.google.com', 65, 19,'pack://siteoforigin:,,,/img/adapterKDF_Multi65.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'EksterniDisk'
GO
INSERT INTO [dbo].[EksterniDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [format], [kapacitet], [masa], [dimenzije], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('WD', 'WDBS4B0020BYL-WESN', 8636441, 10990.0, 5, 2.0, 'www.google.com', 2.5, '2 TB', 0.2, '110.0x81.5x21.5 mm', 'USB 3.0', 'pack://siteoforigin:,,,/img/eksterniDisk_WD_WDBS4B0020BYL-WESN.jpg', GETDATE())
INSERT INTO [dbo].[EksterniDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [format], [kapacitet], [masa], [dimenzije], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Transcend', 'TS1TSJ25C3N', 6818833, 7990.0, 4, 3.0, 'www.google.com', 2.5, '1 TB', 0.136, '114.5x78.5x9.9 5mm', 'USB 3.1 Gen 1 / USB 3.0 interface', 'pack://siteoforigin:,,,/img/eksterniDisk_Transcend_TS1TSJ25C3N.jpg', GETDATE())
INSERT INTO [dbo].[EksterniDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [format], [kapacitet], [masa], [dimenzije], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('WD', 'WDBWLG0060HBK-EESN', 8622665, 23990.0, 3, 2.0, 'www.google.com', 3.5, '6 TB', 0.95, '135x48x165.8 mm', 'USB 3.0', 'pack://siteoforigin:,,,/img/eksterniDisk_WD_WDBWLG0060HBK-EESN.jpg', '2018-11-14')
INSERT INTO [dbo].[EksterniDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [format], [kapacitet], [masa], [dimenzije], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Toshiba', 'HDTB410EK3AA', 8622187, 5990.0, 2, 1.5, 'www.google.com', 2.5, '1 TB', 0.149, '109x78x14 mm', 'USB 3.0', 'pack://siteoforigin:,,,/img/eksterniDisk_Toshiba_HDTB410EK3AA.jpg', GETDATE())
INSERT INTO [dbo].[EksterniDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [format], [kapacitet], [masa], [dimenzije], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Freecom', '35610', 6779160, 5990.0, 1, 2.0, 'www.google.com', 2.5, '1 TB', 0.140, '115x76x12mm', 'USB 3.0', 'pack://siteoforigin:,,,/img/eksterniDisk_Freecom_35610.jpg', '2018-12-05')

EXECUTE [dbo].[ResetujTabelu] @tabela = 'HardDisk'
GO
INSERT INTO [dbo].[HardDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [format], [povezivanje], [kapacitet], [bafer], [obrtaji], [path], [datum_poslednje_kupovine]) VALUES ('Toshiba', 'HDWJ105UZSVA', 6785298, 6990.0, 5, 2.0, 'www.google.com', 'HDD', 2.5, 'SATA 2', '500 GB', '8 MB', '5400rpm', 'pack://siteoforigin:,,,/img/harddiskToshiba_HDWJ105UZSVA.jpg', GETDATE())
INSERT INTO [dbo].[HardDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [format], [povezivanje], [kapacitet], [bafer], [obrtaji], [path], [datum_poslednje_kupovine]) VALUES ('WD', 'WD10JFCX', 1184153, 9990.0, 4, 2.0, 'www.google.com', 'HDD', 2.5, 'SATA 3', '1 TB', '16 MB', '5400rpm', 'pack://siteoforigin:,,,/img/harddiskWD_HDWJ105UZSVA.jpg', '2018-11-19')
INSERT INTO [dbo].[HardDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [format], [povezivanje], [kapacitet], [bafer], [obrtaji], [path], [datum_poslednje_kupovine]) VALUES ('Kingston', 'SA400S37/120G', 6812778, 3490.0, 3, 2.0, 'www.google.com', 'SSD', NULL, 'SATA 3', '120 GB', NULL, '500MB/s / 320MB/s', 'pack://siteoforigin:,,,/img/harddiskKingston_SA400S37120G.jpg', GETDATE())
INSERT INTO [dbo].[HardDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [format], [povezivanje], [kapacitet], [bafer], [obrtaji], [path], [datum_poslednje_kupovine]) VALUES ('WD', 'WDS100T2B0A', 8619428, 25990.0, 2, 2.0, 'www.google.com', 'SSD', NULL, 'SATA 3', '1 TB', NULL, '560MB/s / 530MB/s.', 'pack://siteoforigin:,,,/img/harddiskWD_WDS100T2B0A.jpg', '2018-11-24')
INSERT INTO [dbo].[HardDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [format], [povezivanje], [kapacitet], [bafer], [obrtaji], [path], [datum_poslednje_kupovine]) VALUES ('SanDisk', 'SDSSDA-240G-G26', 6793101, 5990.0, 1, 2.5, 'www.google.com', 'SSD', NULL, 'SATA 3', '240 GB', NULL, '530Mbs / 440Mbs', 'pack://siteoforigin:,,,/img/hardiskSandDisk_SDSSDA-240G-G26.jpg', '2019-01-03')

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Kabl'
GO
INSERT INTO [dbo].[Kabl] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [strana1], [strana2], [duzina], [napomena], [path], [datum_poslednje_kupovine]) VALUES ('Linkom', NULL, 6806960, 359.0, 5, 2.0, 'www.google.com', 'USB 2.0', 'Micro USB', '1 m', NULL, 'pack://siteoforigin:,,,/img/kablLinkom_6806960.jpg', GETDATE())
INSERT INTO [dbo].[Kabl] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [strana1], [strana2], [duzina], [napomena], [path], [datum_poslednje_kupovine]) VALUES ('Linkom', NULL, 1114198, 220.0, 4, 2.5, 'www.google.com', 'USB A muski', 'USB A zenski', '1.8 m', NULL, 'pack://siteoforigin:,,,/img/kablLinkom_1114198.jpg', GETDATE())
INSERT INTO [dbo].[Kabl] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [strana1], [strana2], [duzina], [napomena], [path], [datum_poslednje_kupovine]) VALUES ('Linkom', NULL, 1120003, 579.0, 3, 1.0, 'www.google.com', 'HDMI muski', 'HDMI muski', '1.8 m', NULL, 'pack://siteoforigin:,,,/img/kablLinkom_1120003.jpg', GETDATE())
INSERT INTO [dbo].[Kabl] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [strana1], [strana2], [duzina], [napomena], [path], [datum_poslednje_kupovine]) VALUES ('Linkom', NULL, 1124400, 629.0, 2, 2.0, 'www.google.com', 'VGA muski', 'VGA muski', '3 m', NULL, 'pack://siteoforigin:,,,/img/kablLinkom_1124400.jpg', GETDATE())
INSERT INTO [dbo].[Kabl] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [strana1], [strana2], [duzina], [napomena], [path], [datum_poslednje_kupovine]) VALUES ('Linkom', 'UTP', 1126584, 849.0, 1, 2.0, 'www.google.com', 'Mrezni', 'Mrezni', '15 m', 'UTP Patch kabal', 'pack://siteoforigin:,,,/img/kablLinkom_1126584.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Mikrofon'
GO
INSERT INTO [dbo].[Mikrofon] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [duzina], [frekvencija], [boja], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Hama', 'DM-60 46060', 1147640, 2690.0, 5, 2.0, 'www.google.com', 3, '100 Hz - 10 kHz', 'Crna', '6.35 mm Jack Plug/XLR Socket', 'pack://siteoforigin:,,,/img/mikrofonHAMA_DM60.jpg', GETDATE())
INSERT INTO [dbo].[Mikrofon] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [duzina], [frekvencija], [boja], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Audio-Technica', 'AT2020USB+', 4763152, 23590.0, 4, 2.0, 'www.google.com', NULL, '44.1/48 kHz', 'Crna', NULL, 'pack://siteoforigin:,,,/img/mikrofonAudioTechnica_AT2020.jpg', GETDATE())
INSERT INTO [dbo].[Mikrofon] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [duzina], [frekvencija], [boja], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('ACME', 'A016353', 6822551, 990.0, 3, 1.0, 'www.google.com', NULL, '20 Hz – 16 kHz', 'Crna', '3,5 mm jack', 'pack://siteoforigin:,,,/img/mikrofonACME_A016353.jpg', '2018-12-04')
INSERT INTO [dbo].[Mikrofon] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [duzina], [frekvencija], [boja], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Pioneer', 'DM-DV20', 1193242, 4290.0, 2, 2.5, 'www.google.com', 3, '170Hz-14kHz', 'Crna', '3.5mm 2P mini', 'pack://siteoforigin:,,,/img/mikrofonPioneer_DM-DV20.jpg', GETDATE())
INSERT INTO [dbo].[Mikrofon] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [duzina], [frekvencija], [boja], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Denver', 'KMS-10', 8617170, 3999.0, 1, 2.0, 'www.google.com', NULL, NULL, 'Plava', 'Bluetooth', 'pack://siteoforigin:,,,/img/mikrofonDenver_KMS-10.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Mis'
GO
INSERT INTO [dbo].[Mis] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('HP', 'X3500', 1186238, 2150.0, 5, 2.0, 'www.google.com', 'Opticki', 'pack://siteoforigin:,,,/img/mis_HP_X3500.jpg', GETDATE())
INSERT INTO [dbo].[Mis] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('Hama', 'uRage Reaper 3090', 4713063, 4290.0, 4, 2.0, 'www.google.com', 'Opticki', 'pack://siteoforigin:,,,/img/mis_Hama_uRageReaper3090.jpg', GETDATE())
INSERT INTO [dbo].[Mis] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('MARVO', 'M416+G1', 6801813, 1790.0, 3, 2.0, 'www.google.com', 'Opticki', 'pack://siteoforigin:,,,/img/mis_MARVO_M416+.jpg', GETDATE())
INSERT INTO [dbo].[Mis] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('Rapoo', '3510 RP16978', 6808860, 1690.0, 2, 2.0, 'www.google.com', 'Opticki', 'pack://siteoforigin:,,,/img/mis_Rapoo_3510RP16978.jpg', GETDATE())
INSERT INTO [dbo].[Mis] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('Hama', 'AM-8100', 8623974, 999.0, 1, 2.0, 'www.google.com', 'Opticki', 'pack://siteoforigin:,,,/img/mis_Hama_AM-8100.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Monitor'
GO
INSERT INTO [dbo].[Monitor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [refreshRate], [rezolucija], [path], [datum_poslednje_kupovine]) VALUES ('LG', '19M38A-B', 6776921, 7490.0, 5, 2.0, 'www.google.com', 'TN', 18, 60, '1366x768', 'pack://siteoforigin:,,,/img/monitorLG_19M38A-B.jpg', GETDATE())
INSERT INTO [dbo].[Monitor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [refreshRate], [rezolucija], [path], [datum_poslednje_kupovine]) VALUES ('HP', 'EliteDisplay E190i (E4U30A8)', 1190951, 20190.0, 5, 2.0, 'www.google.com', 'IPS', 19, NULL, NULL, 'pack://siteoforigin:,,,/img/monitorHP_EliteDisplay E190i (E4U30A8).jpg', '2018-11-30')
INSERT INTO [dbo].[Monitor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [refreshRate], [rezolucija], [path], [datum_poslednje_kupovine]) VALUES ('Philips', '223V5LSB2/10', 1181042, 9990.0, 5, 2.0, 'www.google.com', 'TFT-LCD W-LED', 21, 60, '1920x1080', 'pack://siteoforigin:,,,/img/monitorPhilips_223V5LSB210.jpg', GETDATE())
INSERT INTO [dbo].[Monitor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [refreshRate], [rezolucija], [path], [datum_poslednje_kupovine]) VALUES ('Asus', 'VC239HE', 8619684, 18990.0, 5, 2.0, 'www.google.com', 'IPS', 23, NULL, '1920x1080', 'pack://siteoforigin:,,,/img/monitorAsus_VC239HE.jpg', '2018-12-08')
INSERT INTO [dbo].[Monitor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [refreshRate], [rezolucija], [path], [datum_poslednje_kupovine]) VALUES ('Lenovo', 'E24-61B7JAT6EU', 8625840, 18990.0, 5, 2.0, 'www.google.com', 'IPS', 24, 60, '1920x1080 (Full HD)', 'pack://siteoforigin:,,,/img/monitorLenovo_E24-61B7JAT6EU.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Podloga'
GO
INSERT INTO [dbo].[Podloga] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('Hama', '54769', 6763679, 159.0, 5, 2.0, 'www.google.com', 'Obicna', '22,3x18,3x0.6', 'pack://siteoforigin:,,,/img/podlogaHama_6763679.jpg', GETDATE())
INSERT INTO [dbo].[Podloga] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('Genesis', 'NPG-0657', 6780047, 590.0, 5, 2.0, 'www.google.com', 'Obicna', NULL, 'pack://siteoforigin:,,,/img/podlogaGenesis_6780047.jpg', GETDATE())
INSERT INTO [dbo].[Podloga] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('Cyborg', '	G.L.I.D.E. 7', 1190150, 4999.0, 5, 2.0, 'www.google.com', 'Gaming', NULL, 'pack://siteoforigin:,,,/img/podlogaCyborg_1190150.jpg', GETDATE())
INSERT INTO [dbo].[Podloga] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('Hama', 'uRAGE', 4750189, 1190.0, 5, 2.0, 'www.google.com', 'Gaming', '22,8x35,2x0.4', 'pack://siteoforigin:,,,/img/podlogaHama_4750189.jpg', '2019-01-01')
INSERT INTO [dbo].[Podloga] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('Marvo', 'G1', 6772676, 632.0, 5, 2.0, 'www.google.com', 'Gaming', '287x244x3', 'pack://siteoforigin:,,,/img/podlogaMarvo_6772676.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Projektor'
GO
INSERT INTO [dbo].[Projektor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [rezolucija], [osvetljenje], [kontrast], [povezivanje], [zvucniciSnaga], [zvucniciTip], [path], [datum_poslednje_kupovine]) VALUES ('Epson', 'EB-S05', 6827053, 46990.0, 5, 2.0, 'www.google.com', '3LCD', NULL, '800 x 600', '3200Ansi', '15,000 : 1', 'USB, HDMI, VGA', NULL, NULL, 'pack://siteoforigin:,,,/img/projektorEpson_EB-S05.jpg', GETDATE())
INSERT INTO [dbo].[Projektor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [rezolucija], [osvetljenje], [kontrast], [povezivanje], [zvucniciSnaga], [zvucniciTip], [path], [datum_poslednje_kupovine]) VALUES ('SONY', 'VPL-EX435XGA', 8610035, 67090.0, 5, 2.0, 'www.google.com', 'D', NULL, '1024x768', '3200Ansi', '20000:1', 'Mini D-sub, Stereo mini jack, HDMI, RJ-45, USB', 16, NULL, 'pack://siteoforigin:,,,/img/projektorSONY_VPL-EX435XGA.jpg', GETDATE())
INSERT INTO [dbo].[Projektor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [rezolucija], [osvetljenje], [kontrast], [povezivanje], [zvucniciSnaga], [zvucniciTip], [path], [datum_poslednje_kupovine]) VALUES ('Acer', 'X118', 8616237, 47990.0, 5, 2.0, 'www.google.com', 'DLP', NULL, '800 x 600', '3600Ansi', '20,000 : 1', 'USB, VGA', NULL, NULL, 'pack://siteoforigin:,,,/img/projektorAcer_X118.jpg', GETDATE())
INSERT INTO [dbo].[Projektor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [rezolucija], [osvetljenje], [kontrast], [povezivanje], [zvucniciSnaga], [zvucniciTip], [path], [datum_poslednje_kupovine]) VALUES ('LG', 'PW1000G', 8616440, 79000.0, 5, 2.0, 'www.google.com', 'DLP LED', NULL, '1280x800', '1000Ansi', '100,000 : 1', 'RGB in, RS-232C, HDMI (MHL), USB (Type A)', 3, '3W + 3W', 'pack://siteoforigin:,,,/img/projektorLG_VPL-DX221.jpg', '2018-12-15')
INSERT INTO [dbo].[Projektor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [rezolucija], [osvetljenje], [kontrast], [povezivanje], [zvucniciSnaga], [zvucniciTip], [path], [datum_poslednje_kupovine]) VALUES ('Sony', 'VPL-DX221', 6824191, 55090.0, 5, 2.0, 'www.google.com', '3LCD', NULL, '1024 x 768', '2800Ansi', '3700:1', 'HDMI', 1, '1 x 1W', 'pack://siteoforigin:,,,/img/projektorSONY_VPL-DX221.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Skener'
GO
INSERT INTO [dbo].[Skener] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('Canon', 'CanoScan LiDE120', 4745392, 7990.0, 5, 2.0, 'www.google.com', NULL, 'A4', 'pack://siteoforigin:,,,/img/skener_Canon_CanoScanLiDE120.jpg', GETDATE())
INSERT INTO [dbo].[Skener] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('Epson', 'DS-530', 6820174, 49390.0, 5, 2.0, 'www.google.com', NULL, 'A4', 'pack://siteoforigin:,,,/img/skener_Epson_DS-530.jpg', GETDATE())
INSERT INTO [dbo].[Skener] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('Epson', 'V600', 1163212, 31900.0, 5, 2.0, 'www.google.com', NULL, 'A4', 'pack://siteoforigin:,,,/img/skener_Epson_V600.jpg', GETDATE())
INSERT INTO [dbo].[Skener] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('Panasonic', 'KV-S1015C', 8617776, 39590.0, 5, 2.0, 'www.google.com', NULL, 'A4', 'pack://siteoforigin:,,,/img/skener_Panasonic_KV-S1015C.jpg', GETDATE())
INSERT INTO [dbo].[Skener] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('Canon', 'P-208II', 8614968, 16990.0, 5, 2.0, 'www.google.com', NULL, 'A4', 'pack://siteoforigin:,,,/img/skener_Canon_P-208II.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Slusalice'
GO
INSERT INTO [dbo].[Slusalice] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [duzina], [frekvencija], [boja], [mikrofon], [path], [datum_poslednje_kupovine]) VALUES ('Audio-technica', 'ATH-CKX5 BK', 1191900, 5590.0, 5, 2.0, 'www.google.com', 'Bubice (In-ear)', 0.6, '15 - 22000Hz', 'Srebrna/Crna', NULL, 'pack://siteoforigin:,,,/img/slusaliceAudioTechnica_ATH-CKX5 BK.jpg', GETDATE())
INSERT INTO [dbo].[Slusalice] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [duzina], [frekvencija], [boja], [mikrofon], [path], [datum_poslednje_kupovine]) VALUES ('Buxton', 'BHP 6020', 6791330, 2990.0, 5, 2.0, 'www.google.com', 'Bubice (In-ear)', NULL, '20 Hz - 20 kHz', 'Crna', NULL, 'pack://siteoforigin:,,,/img/slusaliceBuxton_BHP 6020.jpg', '2018-12-24')
INSERT INTO [dbo].[Slusalice] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [duzina], [frekvencija], [boja], [mikrofon], [path], [datum_poslednje_kupovine]) VALUES ('Puro ', '500', 8622584, 2690.0, 5, 2.0, 'www.google.com', 'Bezicna bluetooth slucalica', NULL, NULL, 'Crna', 'Da', 'pack://siteoforigin:,,,/img/slusalicePuro_500.jpg', GETDATE())
INSERT INTO [dbo].[Slusalice] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [duzina], [frekvencija], [boja], [mikrofon], [path], [datum_poslednje_kupovine]) VALUES ('Sound Blaster', 'Tactic 3D Rage', 6772152, 9179.0, 5, 2.0, 'www.google.com', 'PC Gaming', 2.2, '20-20000 Hz', 'Crvena/Crna', 'Da', 'pack://siteoforigin:,,,/img/slusaliceSoundBlaster_Tactic 3D Rage.jpg', '2019-01-02')
INSERT INTO [dbo].[Slusalice] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [duzina], [frekvencija], [boja], [mikrofon], [path], [datum_poslednje_kupovine]) VALUES ('Marvo', 'HG8802', 6817073, 3590.0, 5, 2.0, 'www.google.com', 'PC Gaming', 2.2, '15 - 22000Hz', 'Crna', 'Da', 'pack://siteoforigin:,,,/img/slusaliceMarvo_HG8802.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Stampac'
GO
INSERT INTO [dbo].[Stampac] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('Epson', 'Inkjet mono', 1166681, 12990.0, 5, 2.0, 'www.google.com', 'Laserski', 'pack://siteoforigin:,,,/img/stampacEpson_1166681.jpg', GETDATE())
INSERT INTO [dbo].[Stampac] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('Samsung', 'SL-M2026', 4762006, 6790.0, 5, 2.0, 'www.google.com', 'Laserski', 'pack://siteoforigin:,,,/img/stampacSamsung_4762006.jpg', GETDATE())
INSERT INTO [dbo].[Stampac] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('Xerox', '3610v_DN', 6816243, 46990.0, 5, 2.0, 'www.google.com', 'Laserski', 'pack://siteoforigin:,,,/img/stampacXerox_6816243.jpg', GETDATE())
INSERT INTO [dbo].[Stampac] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('Lexmark', 'MS911de', 6813832, 382090.0, 5, 2.0, 'www.google.com', 'Laserski', 'pack://siteoforigin:,,,/img/stampacLexmark_6813832.jpg', GETDATE())
INSERT INTO [dbo].[Stampac] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('HP', 'M102a', 6802811, 8490.0, 5, 2.0, 'www.google.com', 'Laserski', 'pack://siteoforigin:,,,/img/stampacHP_6802811.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Tastatura'
GO
INSERT INTO [dbo].[Tastatura] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('S-box', 'K-18', 8637172, 1290.0, 5, 2.0, 'www.google.com', 'Zicana', 'pack://siteoforigin:,,,/img/tastaturaS-box_K-18.jpg', GETDATE())
INSERT INTO [dbo].[Tastatura] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('Logitech', 'Craft', 6826643, 24990.0, 5, 2.0, 'www.google.com', 'Bezicna', 'pack://siteoforigin:,,,/img/tastaturaLogitech_Craft.jpg', GETDATE())
INSERT INTO [dbo].[Tastatura] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('Altos', 'AL-GKB8161', 6796503, 2490.0, 5, 2.0, 'www.google.com', 'Gamer Zicana', 'pack://siteoforigin:,,,/img/tastaturaAltos_AL-GKB8161.jpg', '2018-12-16')
INSERT INTO [dbo].[Tastatura] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('Fury', 'Hellfire Backlight NFU-0867', 6817974, 1890.0, 5, 2.0, 'www.google.com', 'Zicana', 'pack://siteoforigin:,,,/img/tastaturaFury_Hellfire.jpg', GETDATE())
INSERT INTO [dbo].[Tastatura] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('Hama', 'Urage 113755', 6819358, 3590.0, 5, 2.0, 'www.google.com', 'Zicana', 'pack://siteoforigin:,,,/img/tastaturaHama_uRage113755.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'USBMemorija'
GO
INSERT INTO [dbo].[USBMemorija] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [ulaz], [kapacitet], [boja], [path], [datum_poslednje_kupovine]) VALUES ('SanDisk', 'Ultra Flair', 8625153, 7090.0, 5, 2.0, 'www.google.com', 'USB 3.0', '128 GB', 'Siva', 'pack://siteoforigin:,,,/img/usb_SanDisk_UltraFlair.jpg', GETDATE())
INSERT INTO [dbo].[USBMemorija] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [ulaz], [kapacitet], [boja], [path], [datum_poslednje_kupovine]) VALUES ('Integral', '105596', 6817809, 690.0, 5, 2.0, 'www.google.com', 'USB 2.0', '16 GB', 'Crna', 'pack://siteoforigin:,,,/img/usb_Integral_105596.jpg', GETDATE())
INSERT INTO [dbo].[USBMemorija] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [ulaz], [kapacitet], [boja], [path], [datum_poslednje_kupovine]) VALUES ('Verbatim', 'Pin Stripe', 1169309, 990.0, 5, 2.0, 'www.google.com', 'USB 2.0', '16 GB', 'Crna', 'pack://siteoforigin:,,,/img/usb_Verbatim_PinStripe.jpg', '2019-01-01')
INSERT INTO [dbo].[USBMemorija] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [ulaz], [kapacitet], [boja], [path], [datum_poslednje_kupovine]) VALUES ('Kingston', 'DTVP30DM/16GB', 8626994, 12190.0, 5, 2.0, 'www.google.com', 'USB 3.0', '16 GB', 'Plava', 'pack://siteoforigin:,,,/img/usb_Kingston_DTVP30DM16GB.jpg', GETDATE())
INSERT INTO [dbo].[USBMemorija] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [ulaz], [kapacitet], [boja], [path], [datum_poslednje_kupovine]) VALUES ('Kingston', 'DTMC3/16GB', 4762077, 1490.0, 5, 2.0, 'www.google.com', 'USB 3.1', '16 GB', 'Siva', 'pack://siteoforigin:,,,/img/usb_Kingston_DTMC316GB.jpg', GETDATE())

EXECUTE [dbo].[ResetujTabelu] @tabela = 'Zvucnik'
GO
INSERT INTO [dbo].[Zvucnik] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [sistem], [izlaznaSnaga], [frekvencija], [ulazSlusalice], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Xiaomi', 'Mi Pocket Speaker 2', 8637097, 4490.0, 5, 2.0, 'www.google.com', NULL, '60 x 60 x 93.4mm', '1.1', 5, '180Hz-18KHz', NULL, 'Bluetooth', 'pack://siteoforigin:,,,/img/zvucnikXiaomi_Mi Pocket Speaker 2.jpg', GETDATE())
INSERT INTO [dbo].[Zvucnik] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [sistem], [izlaznaSnaga], [frekvencija], [ulazSlusalice], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Energy', 'Tower 5', 8636412, 11590.0, 5, 2.0, 'www.google.com', NULL, '1000x150x150mm', '2.1', 60, '20Hz - 20kHz', NULL, 'Bluetooth', 'pack://siteoforigin:,,,/img/zvucnikEnergy_Tower 5.jpg', GETDATE())
INSERT INTO [dbo].[Zvucnik] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [sistem], [izlaznaSnaga], [frekvencija], [ulazSlusalice], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Sony', 'SRS-XB31L', 8619504, 17490.0, 5, 2.0, 'www.google.com', NULL, '231mm x 87mm x 81mm', '1.0', NULL, '20Hz – 20.000 Hz', 'Da', 'Bluetooth', 'pack://siteoforigin:,,,/img/zvucnikSony_SRS-XB31L.jpg', '2018-12-01')
INSERT INTO [dbo].[Zvucnik] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [sistem], [izlaznaSnaga], [frekvencija], [ulazSlusalice], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Genius', 'SP-HF160', 8610583, 1190.0, 5, 2.0, 'www.google.com', NULL, '84x129x84mm', '2.0', 4, '160Hz-18KHz', 'Ne', '3.5mm', 'pack://siteoforigin:,,,/img/zvucniciGenius_SP-HF160.jpg', GETDATE())
INSERT INTO [dbo].[Zvucnik] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [sistem], [izlaznaSnaga], [frekvencija], [ulazSlusalice], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('Logitech', 'Z506', 1131946, 11990.0, 5, 2.0, 'www.google.com', NULL, NULL, '5.1', 75, NULL, 'Da', '3x3.5mm (5.1), 2xRCA(emulirani stereo na 5.1)', 'pack://siteoforigin:,,,/img/zvucniciLogitech_Z506.jpg', GETDATE())