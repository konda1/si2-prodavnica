﻿CREATE TABLE [dbo].[USBMemorija]
(
	[id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [proizvodjac] VARCHAR(50) NULL, 
    [ime] VARCHAR(50) NULL, 
    [sifra] INT NULL, 
    [cena] REAL NOT NULL, 
	[stanje] INT NOT NULL,
	[garancija] REAL NULL,
	[link] VARCHAR(100) NULL,
    [ulaz] NCHAR(10) NULL,
	[kapacitet] NCHAR(10) NULL,
	[boja] NCHAR(10) NULL,
	[path] VARCHAR(200) NULL, 
    [datum_poslednje_kupovine] DATE NULL
	CONSTRAINT [USB_provera_cene_i_stanja] CHECK ([cena] >= 0 AND [stanje] >= 0)
)

GO

CREATE TRIGGER [dbo].[After_USBMemorija_Insert]
    ON [dbo].[USBMemorija]
    FOR INSERT
    AS
    BEGIN
        SET NoCount ON
		INSERT INTO [dbo].[NabavniProizvod]
			([tabela], [id_u_tabeli])
		SELECT 'USBMemorija', [id]
		FROM inserted
	END

GO

CREATE TRIGGER [dbo].[After_USBMemorija_Delete]
    ON [dbo].[USBMemorija]
    FOR DELETE
    AS
    BEGIN
        SET NoCount ON
		DELETE FROM [dbo].[NabavniProizvod]
		WHERE tabela='USBMemorija' AND
			id_u_tabeli IN (SELECT deleted.id FROM deleted)
	END

GO

CREATE TRIGGER [dbo].[USBMemorija_Promena_stanja]
ON [dbo].[USBMemorija]
AFTER UPDATE 
AS IF UPDATE([stanje])
BEGIN
   UPDATE [dbo].[USBMemorija]
   SET [datum_poslednje_kupovine] = GETDATE()
   FROM Inserted i, deleted d
   WHERE [dbo].[USBMemorija].[id] = i.[id]  AND (d.[stanje] > i.[stanje])
END
GO

CREATE TRIGGER [dbo].[USBMemorija_Promena_datuma]
ON [dbo].[USBMemorija]
AFTER UPDATE 
AS IF UPDATE([datum_poslednje_kupovine])
BEGIN
   DELETE FROM [dbo].[NeprodavaniProizvodi]
   WHERE ([sifra_proizvoda] IN (SELECT inserted.[sifra] FROM inserted))
END
GO