﻿CREATE TABLE [dbo].[Slusalice]
(
	[id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [proizvodjac] VARCHAR(50) NULL, 
    [ime] VARCHAR(50) NULL, 
    [sifra] INT NULL, 
    [cena] REAL NOT NULL, 
	[stanje] INT NOT NULL,
	[garancija] REAL NULL,
	[link] VARCHAR(100) NULL,
    [tip] VARCHAR(50) NULL,
	[duzina] REAL NULL,
	[frekvencija] NCHAR(20) NULL,
	[boja] NCHAR(20) NULL,
	[mikrofon] CHAR(10) NULL,
	[path] VARCHAR(200) NULL, 
    [datum_poslednje_kupovine] DATE NULL
	CONSTRAINT [Slusalice_provera_cene_i_stanja] CHECK ([cena] >= 0 AND [stanje] >= 0)
)

GO

CREATE TRIGGER [dbo].[After_Slusalice_Insert]
    ON [dbo].[Slusalice]
    FOR INSERT
    AS
    BEGIN
        SET NoCount ON
		INSERT INTO [dbo].[NabavniProizvod]
			([tabela], [id_u_tabeli])
		SELECT 'Slusalice', [id]
		FROM inserted
	END

GO

CREATE TRIGGER [dbo].[After_Slusalice_Delete]
    ON [dbo].[Slusalice]
    FOR DELETE
    AS
    BEGIN
        SET NoCount ON
		DELETE FROM [dbo].[NabavniProizvod]
		WHERE tabela='Slusalice' AND
			id_u_tabeli IN (SELECT deleted.id FROM deleted)
	END

GO

CREATE TRIGGER [dbo].[Slusalice_Promena_stanja]
ON [dbo].[Slusalice]
AFTER UPDATE 
AS IF UPDATE([stanje])
BEGIN
   UPDATE [dbo].[Slusalice]
   SET [datum_poslednje_kupovine] = GETDATE()
   FROM Inserted i, deleted d
   WHERE [dbo].[Slusalice].[id] = i.[id]  AND (d.[stanje] > i.[stanje])
END
GO

CREATE TRIGGER [dbo].[Slusalice_Promena_datuma]
ON [dbo].[Slusalice]
AFTER UPDATE 
AS IF UPDATE([datum_poslednje_kupovine])
BEGIN
   DELETE FROM [dbo].[NeprodavaniProizvodi]
   WHERE ([sifra_proizvoda] IN (SELECT inserted.[sifra] FROM inserted))
END
GO