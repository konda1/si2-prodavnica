﻿CREATE TABLE [dbo].[Kategorija]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY, 
    [naziv] VARCHAR(50) NOT NULL, 
    CONSTRAINT [AK_Kategorija_naziv] UNIQUE ([naziv])
)
