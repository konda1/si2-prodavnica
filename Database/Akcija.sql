﻿CREATE TABLE [dbo].[Akcija]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[tip_proizvoda] VARCHAR(50) NULL,
	[proizvodjac] VARCHAR(50) NULL,
	[sifra_proizvoda] INT NULL,
	[promena_cene] INT NOT NULL,
	[da_li_je_popust] BIT NOT NULL
)
