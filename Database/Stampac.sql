﻿CREATE TABLE [dbo].[Stampac]
(
	[id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [proizvodjac] VARCHAR(50) NULL, 
    [ime] VARCHAR(50) NULL, 
    [sifra] INT NULL, 
    [cena] REAL NOT NULL, 
	[stanje] INT NOT NULL,
	[garancija] REAL NULL,
	[link] VARCHAR(100) NULL,
    [tip] NCHAR(10) NULL,
	[path] VARCHAR(200) NULL, 
    [datum_poslednje_kupovine] DATE NULL
	CONSTRAINT [Stampac_provera_cene_i_stanja] CHECK ([cena] >= 0 AND [stanje] >= 0)
)

GO

CREATE TRIGGER [dbo].[After_Stampac_Insert]
    ON [dbo].[Stampac]
    FOR INSERT
    AS
    BEGIN
        SET NoCount ON
		INSERT INTO [dbo].[NabavniProizvod]
			([tabela], [id_u_tabeli])
		SELECT 'Stampac', [id]
		FROM inserted
	END

GO

CREATE TRIGGER [dbo].[After_Stampac_Delete]
    ON [dbo].[Stampac]
    FOR DELETE
    AS
    BEGIN
        SET NoCount ON
		DELETE FROM [dbo].[NabavniProizvod]
		WHERE tabela='Stampac' AND
			id_u_tabeli IN (SELECT deleted.id FROM deleted)
	END

GO

CREATE TRIGGER [dbo].[Stampac_Promena_stanja]
ON [dbo].[Stampac]
AFTER UPDATE 
AS IF UPDATE([stanje])
BEGIN
   UPDATE [dbo].[Stampac]
   SET [datum_poslednje_kupovine] = GETDATE()
   FROM Inserted i, deleted d
   WHERE [dbo].[Stampac].[id] = i.[id]  AND (d.[stanje] > i.[stanje])
END
GO

CREATE TRIGGER [dbo].[Stampac_Promena_datuma]
ON [dbo].[Stampac]
AFTER UPDATE 
AS IF UPDATE([datum_poslednje_kupovine])
BEGIN
   DELETE FROM [dbo].[NeprodavaniProizvodi]
   WHERE ([sifra_proizvoda] IN (SELECT inserted.[sifra] FROM inserted))
END
GO