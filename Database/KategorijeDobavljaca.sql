﻿CREATE TABLE [dbo].[KategorijeDobavljaca]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY, 
    [id_dobavljaca] INT NOT NULL, 
    [id_kategorije] INT NOT NULL, 
    CONSTRAINT [FK_KategorijeDobavljaca_Dobavljac] FOREIGN KEY ([id_dobavljaca]) REFERENCES [Dobavljac]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_KategorijeDobavljaca_Kategorija] FOREIGN KEY ([id_kategorije]) REFERENCES [Kategorija]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [AK_KategorijeDobavljaca_iddobavljaca_idkategorije] UNIQUE ([id_dobavljaca], [id_kategorije])
)
