﻿CREATE TABLE [dbo].[Porudzbina]
(
	[id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [id_proizvoda] INT NULL, 
    [kolicina] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_Porudzbina_ToProizvod] FOREIGN KEY ([id_proizvoda]) REFERENCES [NabavniProizvod]([Id]) ON DELETE CASCADE
)
