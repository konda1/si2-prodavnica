﻿CREATE TABLE [dbo].[Dobavljac]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY, 
    [naziv] NVARCHAR(50) NOT NULL, 
    [email] VARCHAR(50) NOT NULL, 
    CONSTRAINT [AK_Dobavljac_email] UNIQUE ([email]) 
)
