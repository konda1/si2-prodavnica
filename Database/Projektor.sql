﻿CREATE TABLE [dbo].[Projektor]
(
	[id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [proizvodjac] VARCHAR(50) NULL, 
    [ime] VARCHAR(50) NULL, 
    [sifra] INT NULL, 
    [cena] REAL NOT NULL, 
	[stanje] INT NOT NULL,
	[garancija] REAL NULL,
	[link] VARCHAR(100) NULL,
    [tip] NCHAR(10) NULL,
	[velicina] NCHAR(10) NULL,
	[rezolucija] VARCHAR(50) NULL,
	[osvetljenje] VARCHAR(50) NULL,
	[kontrast] VARCHAR(50) NULL,
	[povezivanje] VARCHAR(50) NULL,
	[zvucniciSnaga] INT NULL,
	[zvucniciTip] NCHAR(10),
	[path] VARCHAR(200) NULL, 
    [datum_poslednje_kupovine] DATE NULL
	CONSTRAINT [Projektor_provera_cene_i_stanja] CHECK ([cena] >= 0 AND [stanje] >= 0)
)

GO

CREATE TRIGGER [dbo].[After_Projektor_Insert]
    ON [dbo].[Projektor]
    FOR INSERT
    AS
    BEGIN
        SET NoCount ON
		INSERT INTO [dbo].[NabavniProizvod]
			([tabela], [id_u_tabeli])
		SELECT 'Projektor', [id]
		FROM inserted
	END

GO

CREATE TRIGGER [dbo].[After_Projektor_Delete]
    ON [dbo].[Projektor]
    FOR DELETE
    AS
    BEGIN
        SET NoCount ON
		DELETE FROM [dbo].[NabavniProizvod]
		WHERE tabela='Projektor' AND
			id_u_tabeli IN (SELECT deleted.id FROM deleted)
	END

GO

CREATE TRIGGER [dbo].[Projektor_Promena_stanja]
ON [dbo].[Projektor]
AFTER UPDATE 
AS IF UPDATE([stanje])
BEGIN
   UPDATE [dbo].[Projektor]
   SET [datum_poslednje_kupovine] = GETDATE()
   FROM Inserted i, deleted d
   WHERE [dbo].[Projektor].[id] = i.[id]  AND (d.[stanje] > i.[stanje])
END
GO

CREATE TRIGGER [dbo].[Projektor_Promena_datuma]
ON [dbo].[Projektor]
AFTER UPDATE 
AS IF UPDATE([datum_poslednje_kupovine])
BEGIN
   DELETE FROM [dbo].[NeprodavaniProizvodi]
   WHERE ([sifra_proizvoda] IN (SELECT inserted.[sifra] FROM inserted))
END
GO