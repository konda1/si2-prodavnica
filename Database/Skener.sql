﻿CREATE TABLE [dbo].[Skener]
(
	[id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [proizvodjac] VARCHAR(50) NULL, 
    [ime] VARCHAR(50) NULL, 
    [sifra] INT NULL, 
    [cena] REAL NOT NULL, 
	[stanje] INT NOT NULL,
	[garancija] REAL NULL,
	[link] VARCHAR(100) NULL,
    [tip] NCHAR(10) NULL,
	[velicina] NCHAR(10) NULL,
	[path] VARCHAR(200) NULL, 
    [datum_poslednje_kupovine] DATE NULL
	CONSTRAINT [Skener_provera_cene_i_stanja] CHECK ([cena] >= 0 AND [stanje] >= 0)
)

GO

CREATE TRIGGER [dbo].[After_Skener_Insert]
    ON [dbo].[Skener]
    FOR INSERT
    AS
    BEGIN
        SET NoCount ON
		INSERT INTO [dbo].[NabavniProizvod]
			([tabela], [id_u_tabeli])
		SELECT 'Skener', [id]
		FROM inserted
	END

GO

CREATE TRIGGER [dbo].[After_Skener_Delete]
    ON [dbo].[Skener]
    FOR DELETE
    AS
    BEGIN
        SET NoCount ON
		DELETE FROM [dbo].[NabavniProizvod]
		WHERE tabela='Skener' AND
			id_u_tabeli IN (SELECT deleted.id FROM deleted)
	END

GO

CREATE TRIGGER [dbo].[Skener_Promena_stanja]
ON [dbo].[Skener]
AFTER UPDATE 
AS IF UPDATE([stanje])
BEGIN
   UPDATE [dbo].[Skener]
   SET [datum_poslednje_kupovine] = GETDATE()
   FROM Inserted i, deleted d
   WHERE [dbo].[Skener].[id] = i.[id]  AND (d.[stanje] > i.[stanje])
END
GO

CREATE TRIGGER [dbo].[Skener_Promena_datuma]
ON [dbo].[Skener]
AFTER UPDATE 
AS IF UPDATE([datum_poslednje_kupovine])
BEGIN
   DELETE FROM [dbo].[NeprodavaniProizvodi]
   WHERE ([sifra_proizvoda] IN (SELECT inserted.[sifra] FROM inserted))
END
GO