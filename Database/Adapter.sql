﻿CREATE TABLE [dbo].[Adapter]
(
	[id] INT IDENTITY (1,1) NOT NULL PRIMARY KEY, 
    [proizvodjac] VARCHAR(50) NULL, 
    [ime] VARCHAR(50) NULL, 
    [sifra] INT NULL, 
    [cena] REAL NOT NULL,
	[stanje] INT NOT NULL,
	[garancija] REAL NULL,
	[link] VARCHAR(100) NULL,
    [snagaW] INT NULL,
	[izlazniNapon] VARCHAR(50) NULL,
	[path] VARCHAR(200) NULL, 
    [datum_poslednje_kupovine] DATE NULL
	CONSTRAINT [Adapter_provera_cene_i_stanja] CHECK ([cena] >= 0 AND [stanje] >= 0)
)

GO

CREATE TRIGGER [dbo].[After_Adapter_Insert]
    ON [dbo].[Adapter]
    FOR INSERT
    AS
    BEGIN
        SET NoCount ON
		INSERT INTO [dbo].[NabavniProizvod]
			([tabela], [id_u_tabeli])
		SELECT 'Adapter', [id]
		FROM inserted
	END

GO

CREATE TRIGGER [dbo].[After_Adapter_Delete]
    ON [dbo].[Adapter]
    FOR DELETE
    AS
    BEGIN
        SET NoCount ON
		DELETE FROM [dbo].[NabavniProizvod]
		WHERE tabela='Adapter' AND
			id_u_tabeli IN (SELECT deleted.id FROM deleted)
	END

GO

CREATE TRIGGER [dbo].[Adapter_Promena_stanja]
ON [dbo].[Adapter]
AFTER UPDATE 
AS IF UPDATE([stanje])
BEGIN
   UPDATE [dbo].[Adapter]
   SET [datum_poslednje_kupovine] = GETDATE()
   FROM Inserted i, deleted d
   WHERE ([dbo].[Adapter].[id] = i.[id]) AND (d.[stanje] > i.[stanje])
END
GO

CREATE TRIGGER [dbo].[Adapter_Promena_datuma]
ON [dbo].[Adapter]
AFTER UPDATE 
AS IF UPDATE([datum_poslednje_kupovine])
BEGIN
   DELETE FROM [dbo].[NeprodavaniProizvodi]
   WHERE ([sifra_proizvoda] IN (SELECT inserted.[sifra] FROM inserted))
END
GO