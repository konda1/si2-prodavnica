﻿CREATE PROCEDURE [dbo].[ResetujTabelu]
	@tabela varchar(50)
AS
	DECLARE @SqlCommand varchar(255)
	DECLARE @SqlCommand2 varchar(255)
	DBCC CHECKIDENT (@tabela, RESEED, 0)

	SET @SqlCommand = CONCAT('DELETE FROM [', @tabela, ']')
	EXEC (@SqlCommand)
RETURN 0
