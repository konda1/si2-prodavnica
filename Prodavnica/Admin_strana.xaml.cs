﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for Admin_strana.xaml
    /// </summary>
    public partial class Admin_strana : Window
    {
        //private DataTable dt = new DataTable("Korisnici");
        public Admin_strana()
        {
            InitializeComponent();
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
            UpdateUserTable();
            
        }

        private void UpdateUserTable()
        {
            string query = String.Format("SELECT [Id], [tip_korisnika], [username], [ime], [prezime] FROM [dbo].[User]");
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    DataTable dt = new DataTable("Korisnici");
                    sda.Fill(dt);
                    ///DataGrid.ItemsSource = dt.DefaultView;
                    sda.Update(dt);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    DataGridUser.ItemsSource = dt.DefaultView;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Neuspesno");
                    Console.WriteLine(ex);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DataGridUser.ItemsSource = null;
            string hashPass = MD5Hash(pass.Password);
            BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[User] ([tip_korisnika], [username], [password], [ime], [prezime]) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", tip.Text, user.Text, hashPass, ime.Text, prezime.Text));
            UpdateUserTable();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string hashPass = MD5Hash(izmeniPass.Text);
            if (izmeniPass.Text.Equals(""))
                BazaPodataka.InsertionIntoDB(String.Format("UPDATE [dbo].[User] SET [tip_korisnika] ='{0}' WHERE [username] = '{1}'", izmeniTip.Text, izmeniUser.Text));
            else if (izmeniTip.Text.Equals(""))
                BazaPodataka.InsertionIntoDB(String.Format("UPDATE [dbo].[User] SET [password] ='{0}' WHERE [username] = '{1}'", hashPass, izmeniUser.Text));
            else
                BazaPodataka.InsertionIntoDB(String.Format("UPDATE [dbo].[User] SET [password] ='{0}', [tip_korisnika] = '{2}' WHERE [username] = '{1}'", hashPass, izmeniUser.Text, izmeniTip.Text));

        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;

            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("x2"));
            }
            return sb.ToString();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Adapter]", Convert.ToInt32(adapterSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Adapter] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [snagaW], [izlazniNapon]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', {7}, '{8}')", adapterProizvodjac.Text, adapterModel.Text, Convert.ToInt32(adapterSifra.Text), Convert.ToDouble(adapterCena.Text), Convert.ToInt32(adapterStanje.Text), Convert.ToDouble(adapterGarancija.Text), adapterLink.Text, Convert.ToInt32(adapterSnagaW.Text), adapterIzlazniNapon.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[EksterniDisk]", Convert.ToInt32(eksterniDiskSifra1.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[EksterniDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [format], [kapacitet], [masa], [dimenzije], [povezivanje]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', {7}, '{8}', {9}, '{10}', '{11}')", esterniDiskProizvodjac1.Text, eksterniDiskModel1.Text, Convert.ToInt32(eksterniDiskSifra1.Text), Convert.ToDouble(eskterniDiskCena1.Text), Convert.ToInt32(eksterniDiskStanje1.Text), Convert.ToDouble(eksterniDiskGarancija1.Text), eksterniDiskLink1.Text, Convert.ToDouble(eksterniDiskFormat1.Text), eksterniDiskKapacitet.Text, Convert.ToDouble(eksterniDiskMasa1.Text), eksterniDiskDimenzije.Text, eksterniDiskPovezivanje.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[HardDisk]", Convert.ToInt32(hardDiskSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[HardDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [format], [povezivanje], [kapacitet], [bafer], [obrtaji]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', {9}, '{10}', '{11}', '{12}')", hardDiskProizvodjac.Text, hardDiskModel.Text, Convert.ToInt32(hardDiskSifra.Text), Convert.ToDouble(hardDiskCena.Text), Convert.ToInt32(hardDiskStanje.Text), Convert.ToDouble(hardDiskGarancija.Text), hardDiskLink.Text, hardDiskTip.Text, Convert.ToDouble(hardDiskFormat.Text), hardDiskPovezivanje.Text, hardDiskKapacitet.Text, hardDiskBafer.Text, hardDiskObrtaji.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Kabl]", Convert.ToInt32(kablSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Kabl] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [strana1], [strana2], [duzina], [napomena]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', {9}, '{10}')", kablProizvodjac.Text, kablModel.Text, Convert.ToInt32(kablSifra.Text), Convert.ToDouble(kablCena.Text), Convert.ToInt32(kablStanje.Text), Convert.ToDouble(kablGarancija.Text), kablLink.Text, kablStrana1.Text, kablStrana2.Text, kablDuzina.Text, kablNapomena.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Mikrofon]", Convert.ToInt32(mikrofonSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Mikrofon] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [duzina], [frekvencija], [boja], [povezivanje]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', {7}, '{8}', {9}, '{10}')", mikrofonProizvodjac.Text, mikrofonModel.Text, Convert.ToInt32(mikrofonSifra.Text), Convert.ToDouble(mikrofonCena.Text), Convert.ToInt32(mikrofonStanje.Text), Convert.ToDouble(mikrofonGarancija.Text), mikrofonLink.Text, Convert.ToInt32(mikrofonDuzina.Text), mikrofonFrekvencija.Text, mikrofonBoja.Text, mikrofonPovezivanje.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Mis]", Convert.ToInt32(misSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Mis] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}')", misProizvodjac.Text, misModel.Text, Convert.ToInt32(misSifra.Text), Convert.ToDouble(misCena.Text), Convert.ToInt32(misStanje.Text), Convert.ToDouble(misGarancija.Text), misLink.Text, misTip.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Monitor]", Convert.ToInt32(monitorSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Monitor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [refreshRate], [rezolucija]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', {8}, {9}, '{10}')", monitorProizvodjac.Text, monitorModel.Text, Convert.ToInt32(monitorSifra.Text), Convert.ToDouble(monitorCena.Text), Convert.ToInt32(monitorStanje.Text), Convert.ToDouble(monitorGarancija.Text), monitorLink.Text, monitorTip.Text, Convert.ToInt32(monitorVelicina.Text), Convert.ToInt32(monitorRefreshRate.Text), monitorRezolucija.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Podloga]", Convert.ToInt32(podlogaSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Podloga] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}')", podlogaProizvodjac.Text, podlogaModel.Text, Convert.ToInt32(podlogaSifra.Text), Convert.ToDouble(podlogaCena.Text), Convert.ToInt32(podlogaStanje.Text), Convert.ToDouble(podlogaGarancija.Text), podlogaLink.Text, podlogaTip.Text, podlogaVelicina.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Projektor]", Convert.ToInt32(projektorSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Projektor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [rezolucija], [osvetljenje], [kontrast], [povezivanje], [zvucniciSnaga], [zvucniciTip]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', {13}, '{14}')", projektorProizvodjac.Text, projektorModel.Text, Convert.ToInt32(projektorSifra.Text), Convert.ToDouble(projektorCena.Text), Convert.ToInt32(projektorStanje.Text), Convert.ToDouble(projektorGarancija.Text), projektorLink.Text, projektorTip.Text, projektorVelicina.Text, projektorRezolucija.Text, projektorOsvetljenje.Text, projektorKontrast.Text, projektorPovezivanje.Text, Convert.ToInt32(projektorZvucniciSnaga.Text), projektorZvucniciTip.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Skener]", Convert.ToInt32(skenerSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Skener] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}')", skenerProizvodjac.Text, skenerModel.Text, Convert.ToInt32(skenerSifra.Text), Convert.ToDouble(skenerCena.Text), Convert.ToInt32(skenerStanje.Text), Convert.ToDouble(skenerGarancija.Text), skenerLink.Text, skenerTip.Text, skenerVelicina.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_13(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Slusalice]", Convert.ToInt32(slusaliceSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Slusalice] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [duzina], [frekvencija], [boja], [mikrofon]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', {8}, '{9}', '{10}', '{11}')", slusaliceProizvodjac.Text, slusaliceModel.Text, Convert.ToInt32(slusaliceSifra.Text), Convert.ToDouble(slusaliceCena.Text), Convert.ToInt32(slusaliceStanje.Text), Convert.ToDouble(slusaliceGarancija.Text), slusaliceLink.Text, slusaliceTip.Text, Convert.ToDouble(slusaliceDuzina.Text), slusaliceFrekvencija.Text, slusaliceBoja.Text, slusaliceMikrofon.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_14(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Stampac]", Convert.ToInt32(stampacSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Stampac] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}')", stampacProizvodjac.Text, stampacModel.Text, Convert.ToInt32(stampacSifra.Text), Convert.ToDouble(stampacCena.Text), Convert.ToInt32(stampacStanje.Text), Convert.ToDouble(stampacGarancija.Text), stampacLink.Text, stampacTip.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_15(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Tastatura]", Convert.ToInt32(tastaturaSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Tastatura] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}')", tastaturaProizvodjac.Text, tastaturaModel.Text, Convert.ToInt32(tastaturaSifra.Text), Convert.ToDouble(tastaturaCena.Text), Convert.ToInt32(tastaturaStanje.Text), Convert.ToDouble(tastaturaGarancija.Text), tastaturaLink.Text, tastaturaTip.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_16(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[USBMemorija]", Convert.ToInt32(usbSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[USBMemorija] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [ulaz], [kapacitet], [boja]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', '{9}')", usbProizvodjac.Text, usbModel.Text, Convert.ToInt32(usbSifra.Text), Convert.ToDouble(usbCena.Text), Convert.ToInt32(usbStanje.Text), Convert.ToDouble(usbGarancija.Text), usbLink.Text, usbUlaz.Text, usbKapacitet.Text, usbBoja.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_17(object sender, RoutedEventArgs e)
        {
            if (BazaPodataka.ProveraSifre("[dbo].[Zvucnik]", Convert.ToInt32(zvucnikSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Zvucnik] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [sistem], [izlaznaSnaga], [frekvencija], [ulazSlusalice], [povezivanje]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', '{9}', {10}, '{11}', '{12}', '{13}')", zvucnikProizvodjac.Text, zvucnikModel.Text, Convert.ToInt32(zvucnikSifra.Text), Convert.ToDouble(zvucnikCena.Text), Convert.ToInt32(zvucnikStanje.Text), Convert.ToDouble(zvucnikGarancija.Text), zvucnikLink.Text, zvucnikTip.Text, zvucnikVelicina.Text, zvucnikSistem.Text, Convert.ToDouble(zvucnikIzlaznaSnaga.Text), zvucnikFrekvencija.Text, zvucnikUlazSlusalice.Text, zvucnikPovezivanje.Text));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }


        private void BtnObrisi(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridUser.ItemsSource = null;
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                int id = Int32.Parse(dataRowView[0].ToString());
                String query = String.Format("DELETE FROM [dbo].[User] Where [Id] = {0}", id);
                BazaPodataka.InsertionIntoDB(query);
                UpdateUserTable();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem item = (ComboBoxItem)ComboBoxProizvod.SelectedItem;
            string tablename = item.Content.ToString();
            string query = String.Format("SELECT [id], [proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link] FROM [dbo].[{0}]", tablename);
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    DataTable dt = new DataTable(tablename);
                    sda.Fill(dt);
                    ///DataGrid.ItemsSource = dt.DefaultView;
                    //sda.Update(dt);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    DataGridProizvod.ItemsSource = dt.DefaultView;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Neuspesno");
                    Console.WriteLine(ex);
                }
                
            }

        }

        private void UpdateProductTable(string tablename)
        {
            string query = String.Format("SELECT [id], [proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link] FROM [dbo].[{0}]", tablename);
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    DataTable dt = new DataTable("Proizvodi");
                    sda.Fill(dt);
                    ///DataGrid.ItemsSource = dt.DefaultView;
                    sda.Update(dt);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    dt.AcceptChanges();
                    DataGridProizvod.ItemsSource = dt.DefaultView;
                }
                catch (Exception ex)
                {
                 //   MessageBox.Show("Neuspesno");
                    Console.WriteLine(ex);
                }
            }
        }

        public void BtnObrisiProizvod(object sender, RoutedEventArgs e)
        {
            try
            {
                ComboBoxItem item = (ComboBoxItem)ComboBoxProizvod.SelectedItem;
                string tablename = item.Content.ToString();
                DataGridProizvod.ItemsSource = null;
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                int id = Int32.Parse(dataRowView[0].ToString());
                String query = String.Format("DELETE FROM [dbo].[{0}] Where [Id] = {1}", tablename, id);
                BazaPodataka.InsertionIntoDB(query);
                UpdateProductTable(tablename);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void DataGridProizvod_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            ComboBoxItem item = (ComboBoxItem)ComboBoxProizvod.SelectedItem;
            string tablename = item.Content.ToString();          

            DataGrid dataGrid = sender as DataGrid;
            DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
            DataGridCell RowColumn = dataGrid.Columns[0].GetCellContent(row).Parent as DataGridCell;
            string CellValue = ((TextBlock)RowColumn.Content).Text;
            int id = Int32.Parse(CellValue);

            string query = String.Format("SELECT * FROM [dbo].[{0}] WHERE [id] = {1}", tablename, id);

            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    DataTable dt = new DataTable(tablename);
                    sda.Fill(dt);
                    ///DataGrid.ItemsSource = dt.DefaultView;
                    //sda.Update(dt);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    DataGridIzmeni.ItemsSource = dt.DefaultView;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Neuspesno");
                    Console.WriteLine(ex);
                }

            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ComboBoxItem item = (ComboBoxItem)ComboBoxProizvod.SelectedItem;
            string tablename = item.Content.ToString();

            DataGrid dataGrid = DataGridIzmeni;
            DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
            DataGridCell RowColumn = dataGrid.Columns[0].GetCellContent(row).Parent as DataGridCell;
            string CellValue = ((TextBlock)RowColumn.Content).Text;
            int id = Int32.Parse(CellValue);

            string query = String.Format("SELECT * FROM [dbo].[{0}] WHERE [id] = {1}", tablename, id);
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    
                    DataSet ds= new DataSet("Izmena");
                    sda.Fill(ds);
                    ///DataGrid.ItemsSource = dt.DefaultView;
                    for(int i = 1; i < ds.Tables[0].Columns.Count; i++)
                    {
                        DataGridCell RowColumn1 = dataGrid.Columns[i].GetCellContent(row).Parent as DataGridCell;
                        string CellValue1 = ((TextBlock)RowColumn1.Content).Text;
                        Type t = ds.Tables[0].Columns[i].DataType;
                        if (t == typeof(int))
                        {
                            ds.Tables[0].Rows[0][i] = Int32.Parse(CellValue1);
                        }
                        else if (t == typeof(double))
                        {
                            ds.Tables[0].Rows[0][i] = Double.Parse(CellValue1);
                        }
                        else
                            ds.Tables[0].Rows[0][i] = CellValue1;
                    }

                    SqlCommandBuilder cb = new SqlCommandBuilder(sda);
                    sda.UpdateCommand = cb.GetUpdateCommand();
                    sda.Update(ds);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    UpdateProductTable(tablename);
                    
                    DataGridIzmeni.ItemsSource = null;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }

        }

        private void button_Click_18(object sender, RoutedEventArgs e)
        {
            this.Hide();
            MainWindow mw = new MainWindow();
            mw.Show();
        }
    }
}
