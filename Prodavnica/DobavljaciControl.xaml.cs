﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for DobavljaciControl.xaml
    /// </summary>
    public partial class DobavljaciControl : UserControlBase
    {
        public DobavljaciControl(string title = null) : base(title)
        {
            InitializeComponent();
            RefreshDataGrid();
        }

        public void RefreshDataGrid()
        {
            SetEditing(false);
            DataTable dt = Dobavljaci.NadjiDobavljace();
            dataGridDobavljaci.ItemsSource = dt.DefaultView;
        }

        public int SelectedId { get; set; }

        public void SetEditing(bool enabled)
        {
            buttonIzbrisi.IsEnabled = enabled;
        }

        private void DataGridDobavljaci_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetEditing(true);
            DataRowView item = dataGridDobavljaci.SelectedItem as DataRowView;
            if(item != null)
                SelectedId = Convert.ToInt32(item.Row["Id"].ToString());
        }

        private void ButtonDodaj_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DodavanjeDobavljaca d = new DodavanjeDobavljaca(this);
                d.Show();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Neuspesno");
            }
        }

        private void ButtonIzbrisi_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool uspesno = BazaPodataka.UpdateOrDeleteQuery("DELETE FROM [Dobavljac] WHERE [Id] = @id", new Dictionary<string, object>() { { "@id", SelectedId} }) > 0;
                RefreshDataGrid();
                if(!uspesno)
                    MessageBox.Show("Neuspesno");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Neuspesno");
            }
        }

        private void ButtonKategorije_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IzmenaKategorija kat = new IzmenaKategorija(SelectedId);
                kat.Show();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Neuspesno");
            }
        }
    }
}
