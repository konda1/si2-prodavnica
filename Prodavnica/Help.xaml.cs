﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for Help.xaml
    /// </summary>
    public partial class Help : Window
    {
        private string tipKorisnika;
        private List<HelpStranica> stranice;
        public Help(string tipKorisnika)
        {
            InitializeComponent();
            this.tipKorisnika = tipKorisnika;
            NapraviKopijuStranica();
            ObradiStranice();
            //if (listBox.Items.Count > 0)
            //    listBox.SelectedIndex = 0;
        }

        private void NapraviKopijuStranica()
        {
            stranice = new List<HelpStranica>();
            foreach(HelpStranica stranica in KorisnickoUputstvo.VratiStranicePoPrivilegiji(tipKorisnika))
            {
                stranice.Add(new HelpStranica(stranica));
            }
        }

        private void ObradiStranice()
        {
            int brojacNaslova = 0;
            foreach(HelpStranica stranica in stranice)
            {
                brojacNaslova++;
                string naslovSaBrojem = string.Format("{0}. {1}", brojacNaslova, stranica.Naslov);
                stranica.Naslov = naslovSaBrojem;
                listBox.Items.Add(stranica.Naslov);
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HelpStranica stranica = stranice[listBox.SelectedIndex];
            webBrowser.NavigateToString(stranica.TekstSaNaslovom);
        }
    }
}
