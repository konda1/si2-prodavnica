﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Prodavnica
{
    public class UserControlBase : UserControl
    {
        public string Title { get; set; } = null;
        public UserControlBase(string title = null)
        {
            Title = title;
        }
    }
}
