﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for NeprodavaniProizvodiControl.xaml
    /// </summary>
    public partial class NeprodavaniProizvodiControl : UserControlBase
    {
        public NeprodavaniProizvodiControl(string title) : base(title)
        {
            InitializeComponent();
            ProveraDatuma();
            UpdateNPTable();
            ProveraNeprodavanihProizvoda();
        }

        public NeprodavaniProizvodiControl(int s)
        { 
            Notify();
        }
        
        private void UpdateNPTable()
        {
            string query = String.Format("SELECT * FROM [dbo].[NeprodavaniProizvodi] ORDER BY [aktivno]");
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    DataTable dt = new DataTable("NeprodavaniProizvodi");
                    sda.Fill(dt);
                    ///DataGrid.ItemsSource = dt.DefaultView;
                    sda.Update(dt);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    DataGridNeprodavaniProizvodi.ItemsSource = dt.DefaultView;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Neuspesno");
                    Console.WriteLine(ex);
                }
            }
        }

        static int notifikacija = 0;
        
        public void ProveraNeprodavanihProizvoda()
        {
            foreach(DataRowView dr in DataGridNeprodavaniProizvodi.ItemsSource)
            {
                string aktivno = dr[6].ToString();
                if (aktivno.Equals("False"))
                {
                    notifikacija = 1;
                    break;
                }
            }
        }

        public void Notify()
        {
            if (notifikacija == 1)
            {
                MessageBox.Show("Imate nepotvrdjenih snizenja cena neprodavanih proizvoda!");
            }
        }

        private void BtnPotvrdi(object sender, RoutedEventArgs e)
        {
            DataGrid dataGrid = DataGridNeprodavaniProizvodi;
            DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
            DataGridCell TipProizvoda = dataGrid.Columns[1].GetCellContent(row).Parent as DataGridCell;
            DataGridCell IdUTabeliProizvoda = dataGrid.Columns[2].GetCellContent(row).Parent as DataGridCell;
            DataGridCell StaraCena = dataGrid.Columns[4].GetCellContent(row).Parent as DataGridCell;
            DataGridCell TrenutniPopust = dataGrid.Columns[5].GetCellContent(row).Parent as DataGridCell;
            DataGridCell SifraProizvoda = dataGrid.Columns[3].GetCellContent(row).Parent as DataGridCell;
            string CellValueTip = ((TextBlock)TipProizvoda.Content).Text;
            string CellValueId = ((TextBlock)IdUTabeliProizvoda.Content).Text;
            string CellValuePopust = ((TextBlock)TrenutniPopust.Content).Text;
            string CellValueCena = ((TextBlock)StaraCena.Content).Text;
            string CellValueSifra = ((TextBlock)SifraProizvoda.Content).Text;
            int id = Int32.Parse(CellValueId);
            int popust = Int32.Parse(CellValuePopust);
            double cena = Single.Parse(CellValueCena);
            int sifra = Int32.Parse(CellValueSifra);

            double cenaSaPopustom = (cena * (100 - (double)popust)) / 100;

            if ((Single.Parse(BazaPodataka.ReadFromDB(String.Format("SELECT [cena] FROM [dbo].[{0}] WHERE ([id] = {1})", CellValueTip, id)))) == cenaSaPopustom)
            {
                MessageBox.Show("Snizenje je vec aktivno!");
            }
            else
            {
                BazaPodataka.UpdateDB(String.Format(new System.Globalization.CultureInfo("us-US"), "UPDATE [dbo].[{0}] SET [cena] = {1} WHERE ([id] = {2})", CellValueTip, cenaSaPopustom,  id));
                BazaPodataka.UpdateDB(String.Format("UPDATE [dbo].[NeprodavaniProizvodi] SET [aktivno] = 1 WHERE [sifra_proizvoda] = {0}", sifra));
                MessageBox.Show("Uspesno!");
                UpdateNPTable();
            }
        }

        private void BtnOtkazi(object sender, RoutedEventArgs e)
        {
            DataGrid dataGrid = DataGridNeprodavaniProizvodi;
            DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
            DataGridCell TipProizvoda = dataGrid.Columns[1].GetCellContent(row).Parent as DataGridCell;
            DataGridCell IdUTabeliProizvoda = dataGrid.Columns[2].GetCellContent(row).Parent as DataGridCell;
            DataGridCell StaraCena = dataGrid.Columns[4].GetCellContent(row).Parent as DataGridCell;
            DataGridCell TrenutniPopust = dataGrid.Columns[5].GetCellContent(row).Parent as DataGridCell;
            DataGridCell SifraProizvoda = dataGrid.Columns[3].GetCellContent(row).Parent as DataGridCell;
            string CellValueTip = ((TextBlock)TipProizvoda.Content).Text;
            string CellValueId = ((TextBlock)IdUTabeliProizvoda.Content).Text;
            string CellValuePopust = ((TextBlock)TrenutniPopust.Content).Text;
            string CellValueCena = ((TextBlock)StaraCena.Content).Text;
            string CellValueSifra = ((TextBlock)SifraProizvoda.Content).Text;
            int id = Int32.Parse(CellValueId);
            int popust = Int32.Parse(CellValuePopust);
            double cena = Single.Parse(CellValueCena);
            int sifra = Int32.Parse(CellValueSifra);

            double cenaSaPopustom = (cena * (100 - (double)popust)) / 100;

            if ((Single.Parse(BazaPodataka.ReadFromDB(String.Format("SELECT [cena] FROM [dbo].[{0}] WHERE ([id] = {1})", CellValueTip, id)))) == cenaSaPopustom)
            {
                BazaPodataka.UpdateDB(String.Format(new System.Globalization.CultureInfo("us-US"), "UPDATE [dbo].[{0}] SET [cena] = {1} WHERE ([id] = {2})", CellValueTip, cena, id));
                BazaPodataka.UpdateDB(String.Format("UPDATE [dbo].[NeprodavaniProizvodi] SET [aktivno] = 0 WHERE [sifra_proizvoda] = {0}", sifra));
                MessageBox.Show("Uspesno!");
                UpdateNPTable();
            }
            else
            {
                MessageBox.Show("Nijedno snizenje nije aktivno!");
            }
        }

        string[] tables = { "Adapter", "EksterniDisk", "HardDisk", "Kabl", "Mikrofon", "Mis", "Monitor", "Podloga", "Projektor", "Skener", "Slusalice", "Stampac", "Tastatura", "USBMemorija", "Zvucnik" };


        private void ProveraDatuma()
        {
            for(int i = 0; i<tables.Length; i++)
            {
                string query = BazaPodataka.ReadFromDB(String.Format("SELECT [id] FROM [dbo].[{0}] WHERE ([datum_poslednje_kupovine] <= DATEADD(day, -30, GETDATE()))", tables[i]));
                string[] array = query.Split(' ');
                
                for(int j = 0; j<array.Length; j++)
                {
                    if (!array[j].Equals(""))
                        BazaPodataka.InsertionIntoDBWithoutMessageBox(String.Format("IF NOT EXISTS (SELECT * FROM [dbo].[NeprodavaniProizvodi] WHERE ([sifra_proizvoda] = {2})) INSERT INTO [dbo].[NeprodavaniProizvodi] VALUES ('{0}', {1}, {2}, {3}, 5, 0)", tables[i], array[j], Int32.Parse(BazaPodataka.ReadFromDB(String.Format("SELECT [sifra] FROM [dbo].[{0}] WHERE ([id] = {1})", tables[i], Int32.Parse(array[j])))), Single.Parse(BazaPodataka.ReadFromDB(String.Format("SELECT [cena] FROM [dbo].[{0}] WHERE ([id] = {1})", tables[i], Int32.Parse(array[j]))))));
                }
            }

            string queryTip = BazaPodataka.ReadFromDB("SELECT [tip_proizvoda] FROM [dbo].[NeprodavaniProizvodi]");
            string[] arrayTip = queryTip.Split(' ');
            string queryId = BazaPodataka.ReadFromDB("SELECT [id_u_tabeli_proizvoda] FROM [dbo].[NeprodavaniProizvodi]");
            string[] arrayId = queryId.Split(' ');
            string queryStaraCena = BazaPodataka.ReadFromDB("SELECT [stara_cena] FROM [dbo].[NeprodavaniProizvodi]");
            string[] arrayStaraCena = queryStaraCena.Split(' ');


            for (int i = 0; i < arrayTip.Length; i++)
            {
                if (!arrayTip[i].Equals(""))
                {
                    int dani = Int32.Parse(BazaPodataka.ReadFromDB(String.Format("SELECT DATEDIFF(DAY, [datum_poslednje_kupovine], GETDATE()) FROM [dbo].[{0}] WHERE ([id] = {1})", arrayTip[i], Int32.Parse(arrayId[i]))));
                    if (dani >= 90)
                    {
                        string cena = BazaPodataka.ReadFromDB(String.Format("SELECT [cena] FROM [dbo].[{0}] WHERE [id] = {1}", arrayTip[i], arrayId[i]));
                        double trenutnaCena = Single.Parse(cena);
                        double staraCenaSaPopustom = (Int32.Parse(arrayStaraCena[i]) * (100 - 15)) / 100;
                        if (trenutnaCena != staraCenaSaPopustom)
                        {
                            BazaPodataka.UpdateDB(String.Format("UPDATE [dbo].[NeprodavaniProizvodi] SET [trenutni_popust] = 15, [aktivno] = 0 WHERE ([tip_proizvoda] = '{0}' AND [id_u_tabeli_proizvoda] = {1})", arrayTip[i], Int32.Parse(arrayId[i])));
                        }
                    }
                    else if (dani >= 60)
                    {
                        string cena = BazaPodataka.ReadFromDB(String.Format("SELECT [cena] FROM [dbo].[{0}] WHERE [id] = {1}", arrayTip[i], arrayId[i]));
                        double trenutnaCena = Single.Parse(cena);
                        double staraCenaSaPopustom = (Int32.Parse(arrayStaraCena[i]) * (100 - 10)) / 100;
                        if (trenutnaCena != staraCenaSaPopustom) { 
                            BazaPodataka.UpdateDB(String.Format("UPDATE [dbo].[NeprodavaniProizvodi] SET [trenutni_popust] = 10, [aktivno] = 0 WHERE ([tip_proizvoda] = '{0}' AND [id_u_tabeli_proizvoda] = {1})", arrayTip[i], Int32.Parse(arrayId[i])));
                        }
                    }
                } 
            }
        }
    }
}
