﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for DodavanjeDobavljaca.xaml
    /// </summary>
    public partial class DodavanjeDobavljaca : Window
    {
        private DobavljaciControl parent;
        public DodavanjeDobavljaca(DobavljaciControl parent = null)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private bool DodajDobavljaca()
        {
            try
            {
                bool uspesno = BazaPodataka.UpdateOrDeleteQuery("INSERT INTO [Dobavljac] ([naziv], [email]) VALUES (@naziv, @email)", new Dictionary<string, object>()
                {
                    { "@naziv", textBoxNaziv.Text },
                    { "@email", textBoxEmail.Text },
                }) > 0;
                if (uspesno)
                    parent?.RefreshDataGrid();
                MessageBox.Show(uspesno ? "Uspesno" : "Neuspesno");
                return uspesno;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void Ponisti_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Dalje_Click(object sender, RoutedEventArgs e)
        {
            if(DodajDobavljaca())
            {
                SqlDataReader reader = BazaPodataka.SelectQuery("SELECT [Id] FROM [Dobavljac] WHERE [naziv] = @naziv AND [email] = @email", new Dictionary<string, object>()
                {
                    { "@naziv", textBoxNaziv.Text },
                    { "@email", textBoxEmail.Text },
                });
                if(reader.Read())
                {
                    int id = Convert.ToInt32(reader["Id"].ToString());
                    IzmenaKategorija k = new IzmenaKategorija(id);
                    Hide();
                    k.ShowDialog();
                    Close();
                }
                else
                {
                    MessageBox.Show("Neuspeno");
                }
            }
        }

        private void Kraj_Click(object sender, RoutedEventArgs e)
        {
            if (DodajDobavljaca())
                Close();
        }
    }
}
