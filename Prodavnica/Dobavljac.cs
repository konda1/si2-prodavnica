﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prodavnica
{
    public class Dobavljac
    {
        public int Id { get; private set; }
        public string Naziv { get; set; }
        public string Email { get; set; }

        private static Dobavljac JedanDobavljacIzCitaca(SqlDataReader reader)
        {
            return new Dobavljac
            {
                Id = Convert.ToInt32(reader["id"].ToString()),
                Naziv = reader["naziv"].ToString(),
                Email = reader["email"].ToString(),
            };
        }

        public static Dobavljac Nadji(int id_dobavljaca)
        {
            string query =
                "SELECT [Id], [naziv], [email]" + " " +
                "FROM [Dobavljac]" + " " +
                "WHERE [Dobavljac].[Id] = @id_dobavljaca";
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@id_dobavljaca", id_dobavljaca } };
            SqlDataReader reader = BazaPodataka.SelectQuery(query, parameters);
            if (reader.Read())
            {
                return JedanDobavljacIzCitaca(reader);
            }
            else
            {
                return null;
            }
        }

        public static List<Dobavljac> Nadji(SqlDataReader reader)
        {
            List<Dobavljac> dobavljaci = new List<Dobavljac>();
            while(reader.Read())
            {
                dobavljaci.Add(JedanDobavljacIzCitaca(reader));
            }
            return dobavljaci;
        }
    }
}
