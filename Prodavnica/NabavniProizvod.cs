﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prodavnica
{
    class NabavniProizvod
    {
        public int id { get; private set; }
        public string tabela { get; private set; }
        public int id_u_tabeli { get; private set; }
        public int broj_prodatih { get; set; }

        public bool SacuvajPromeneUBazu()
        {
            string query = "UPDATE [NabavniProizvod] SET [broj_prodatih] = @broj_prodatih WHERE [Id] = @id";
            Dictionary<string, object> parameters = new Dictionary<string, object>()
            {
                { "@id", id },
                { "@broj_prodatih", broj_prodatih },
            };
            return BazaPodataka.UpdateOrDeleteQuery(query, parameters) > 0;
        }

        public static NabavniProizvod NapraviInstancuIzCitaca(SqlDataReader reader)
        {
            return new NabavniProizvod
            {
                id = Convert.ToInt32(reader["id"].ToString()),
                tabela = reader["tabela"].ToString(),
                id_u_tabeli = Convert.ToInt32(reader["id_u_tabeli"].ToString()),
                broj_prodatih = Convert.ToInt32(reader["broj_prodatih"].ToString()),
            };
        }

        public static NabavniProizvod Nadji(string tabela, int id_u_tabeli)
        {
            Proizvodi.EValidacijaKategorije(tabela);
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();

                string query = @"SELECT * FROM [NabavniProizvod] WHERE tabela = @tabela AND id_u_tabeli = @id_u_tabeli";

                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Parameters.AddWithValue("@tabela", tabela);
                    command.Parameters.AddWithValue("@id_u_tabeli", id_u_tabeli);

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        return NapraviInstancuIzCitaca(reader);
                    }
                    return null;
                }
            }
        }

        public static NabavniProizvod Nadji(int id)
        {
            SqlDataReader reader = BazaPodataka.SelectQuery("SELECT * FROM [NabavniProizvod] WHERE [Id] = @id", new Dictionary<string, object> { { "@id", id } });
            if(reader.Read())
            {
                return NapraviInstancuIzCitaca(reader);
            }
            else
            {
                return null;
            }
        }
    }
}
