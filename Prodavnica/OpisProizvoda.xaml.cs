﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for OpisProizvoda.xaml
    /// </summary>
    public partial class OpisProizvoda : Window
    {
        public OpisProizvoda()
        {
            InitializeComponent();
        }

        public OpisProizvoda(string id, string proizvod)
        {
            InitializeComponent();
            DataTable oldTable = new DataTable(proizvod);
            DataTable newTable = new DataTable(proizvod);
            string query = String.Format("SELECT * FROM {0} WHERE [id]={1}", proizvod, id);
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    sda.Fill(oldTable);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    //DataGridOpis.ItemsSource = oldTable.DefaultView;
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Neuspesno");
                    MessageBox.Show(ex.ToString());
                }
            }
            
            newTable.Columns.Add("");
            newTable.Columns.Add("");
            //for (int i = 0; i < oldTable.Rows.Count; i++)
            //    newTable.Columns.Add();

            for (int i = 0; i < oldTable.Columns.Count; i++)
            {
                DataRow newRow = newTable.NewRow();

                newRow[0] = oldTable.Columns[i].Caption;
                for (int j = 0; j < oldTable.Rows.Count; j++)
                    newRow[j + 1] = oldTable.Rows[j][i];
                newTable.Rows.Add(newRow);
            }

            query = String.Format("SELECT [path] FROM {0} WHERE [id]={1}", proizvod, id);
            string path = BazaPodataka.ReadFromDB(query);
            var uriSource = new Uri(@path, UriKind.RelativeOrAbsolute);
            image.Source = new BitmapImage(uriSource);
            DataGridOpis.ItemsSource = newTable.DefaultView;
    
        }

        

        private void DataGridOpis_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
