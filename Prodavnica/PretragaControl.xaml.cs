﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for PretragaControl.xaml
    /// </summary>
    public partial class PretragaControl : UserControlBase
    {
        string tablename { get { return RadnikGlobals.tablename; } set { RadnikGlobals.tablename = value; } }
        string[] proizvodi = { "Adapter", "EksterniDisk", "HardDisk", "Kabl", "Mikrofon", "Mis", "Monitor", "Podloga", "Projektor", "Skener", "Slusalice", "Stampac", "Tastatura", "USBMemorija", "Zvucnik" };
        DataTable tabela = new DataTable();
        bool textPretraga = false;
        public PretragaControl(string title = null) : base(title)
        {
            InitializeComponent();
            //tabela.Columns.Add("Proizvod", typeof(String));
            tabela.Columns.Add("id", typeof(String));
            tabela.Columns.Add("Proizvođač", typeof(String));
            tabela.Columns.Add("Ime proizvoda", typeof(String));
            tabela.Columns.Add("Šifra proizvoda", typeof(String));
            tabela.Columns.Add("Cena", typeof(String));
            tabela.Columns.Add("Stanje", typeof(String));
            tabela.Columns.Add("Garancija", typeof(String));
            tabela.Columns.Add("Proizvod", typeof(String));
            NarucivanjeGlobals.pretragaControl = this;
        }

        public void RefreshDataGrid()
        {
            if (!textPretraga)
            {
                if (string.IsNullOrEmpty(tablename))
                    return;
                DataTable dt = Proizvodi.NadjiProizvodeNaStanju(tablename);
                DataGrid.ItemsSource = dt.DefaultView;
            }
            else
            {
                tabela.Clear();
                string ime = textBox.Text;
                for (int i = 0; i < proizvodi.Length; i++)
                {
                    string id = BazaPodataka.ReadFromDB(String.Format("SELECT [id] FROM {0} WHERE [stanje] > 0 AND ([ime]='{1}' OR [proizvodjac]='{1}')", proizvodi[i], ime));
                    for (int j = 0; j < id.Length; j += 2)
                    {
                        DataRow red = tabela.NewRow();
                        //red[0] = proizvodi[i];
                        red[0] = id[j];
                        red[1] = BazaPodataka.ReadFromDB(String.Format("SELECT [proizvodjac] FROM {0} WHERE [id]='{1}'", proizvodi[i], id[j]));
                        red[2] = BazaPodataka.ReadFromDB(String.Format("SELECT [ime] FROM {0} WHERE [id]='{1}'", proizvodi[i], id[j]));
                        red[3] = BazaPodataka.ReadFromDB(String.Format("SELECT [sifra] FROM {0} WHERE [id]='{1}'", proizvodi[i], id[j]));
                        red[4] = BazaPodataka.ReadFromDB(String.Format("SELECT [cena] FROM {0} WHERE [id]='{1}'", proizvodi[i], id[j]));
                        red[5] = BazaPodataka.ReadFromDB(String.Format("SELECT [stanje] FROM {0} WHERE [id]='{1}'", proizvodi[i], id[j]));
                        red[6] = BazaPodataka.ReadFromDB(String.Format("SELECT [garancija] FROM {0} WHERE [id]='{1}'", proizvodi[i], id[j]));
                        red[7] = proizvodi[i];
                        tabela.Rows.Add(red);
                    }
                }
                DataGrid.ItemsSource = tabela.DefaultView;
                tablename = "";
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            textPretraga = false;
            ComboBoxItem item = (ComboBoxItem)ComboBox.SelectedItem;
            tablename = item.Content.ToString();
            
            try
            {
                RefreshDataGrid();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Neuspesno");
                Console.WriteLine(ex);
            }
        }

        private void BtnKupi(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                double cena = Math.Round((1.21212121 * Double.Parse(dataRowView[4].ToString())), 2);
                DataRow racunRed = RadnikGlobals.tabelaRacun.NewRow();
                if (tablename.Equals(""))
                {
                    tablename = dataRowView["Proizvod"].ToString();
                }
                racunRed[0] = tablename;
                racunRed[1] = dataRowView[3].ToString();
                racunRed[2] = dataRowView[1].ToString();
                racunRed[3] = dataRowView[2].ToString();
                racunRed[4] = cena.ToString();
                RadnikGlobals.tabelaRacun.Rows.Add(racunRed);
                RadnikGlobals.DataGridRacun.ItemsSource = RadnikGlobals.tabelaRacun.DefaultView;
                RadnikGlobals.ukupnaCena += cena;
                RadnikGlobals.textBox.Text = "Vaš račun: " + (Math.Round(RadnikGlobals.ukupnaCena, 2)).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }

        private void BtnOpis(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                OpisProizvoda op = new OpisProizvoda(dataRowView["id"].ToString(), dataRowView["Proizvod"].ToString());
                op.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

}

        private void btnPretraga_Click(object sender, RoutedEventArgs e)
        {
            textPretraga = true;
            RefreshDataGrid();
        }
    }
}
