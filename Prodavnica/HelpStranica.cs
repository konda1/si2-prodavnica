﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prodavnica
{
    public class HelpStranica
    {
        public string Naslov { get; set; }
        public string Tekst { get; set; }
        public string NivoPrivilegije { get; set; }
        public string TekstSaNaslovom => string.Format(@"<h1>{0}</h1>{1}", Naslov, Tekst);

        public HelpStranica(string naslov, string tekst, string nivoPrivilegije = "R")
        {
            Korisnici.EValidacijaPrivilegije(nivoPrivilegije);
            Naslov = naslov;
            Tekst = tekst;
            NivoPrivilegije = nivoPrivilegije;
        }

        public HelpStranica(HelpStranica stranica): this(stranica.Naslov, stranica.Tekst, stranica.NivoPrivilegije)
        {

        }
    }
}
