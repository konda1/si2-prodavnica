﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prodavnica
{
    public static class Dobavljaci
    {
        public static DataTable NadjiDobavljace(string query = "SELECT * FROM [Dobavljac]")
        {
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);

                DataTable dt = new DataTable();
                sda.Fill(dt);
                command.Dispose();
                sda.Dispose();
                conn.Close();
                return dt;
            }
        }

        public static List<string> NadjiKategorijeDobavljaca(int id_dobavljaca)
        {
            SqlDataReader reader = BazaPodataka.SelectQuery(
                "SELECT [Kategorija].[naziv] AS naziv" + " " +
                "FROM [Dobavljac] JOIN [KategorijeDobavljaca] JOIN [Kategorija]" + " " +
                "ON [Kategorija].[Id] = [KategorijeDobavljaca].[id_kategorije]" + " " +
                "ON [KategorijeDobavljaca].[id_dobavljaca] = [Dobavljac].[Id]" + " " +
                "WHERE [Dobavljac].[Id] = @id_dobavljaca",
                new Dictionary<string, object> { {"@id_dobavljaca", id_dobavljaca } }
            );
            return Utils.GetValuesListFromReader<string>(reader, "naziv");
        }

        public static List<Dobavljac> NadjiDobavljaceKategorije(int id_kategorije)
        {
            SqlDataReader reader = BazaPodataka.SelectQuery(
                "SELECT [Dobavljac].[Id] AS Id, [Dobavljac].[naziv] AS naziv, [Dobavljac].[email] AS email" + " " +
                "FROM [Dobavljac] JOIN [KategorijeDobavljaca] JOIN [Kategorija]" + " " +
                "ON [Kategorija].[Id] = [KategorijeDobavljaca].[id_kategorije]" + " " +
                "ON [KategorijeDobavljaca].[id_dobavljaca] = [Dobavljac].[Id]" + " " +
                "WHERE [Kategorija].[Id] = @id_kategorije",
                new Dictionary<string, object> { { "@id_kategorije", id_kategorije } }
            );
            return Dobavljac.Nadji(reader);
        }

        public static void PostaviKategorijeZaDobavljaca(int id_dobavljaca, List<string> kategorije)
        {
            string delete_query = "DELETE FROM [KategorijeDobavljaca] WHERE [id_dobavljaca] = @id_dobavljaca";
            BazaPodataka.UpdateOrDeleteQuery(delete_query, new Dictionary<string, object>() { { "@id_dobavljaca", id_dobavljaca } });

            if (kategorije.Count > 0)
            {
                string update_query =
                    "INSERT INTO [KategorijeDobavljaca]" + " " +
                    "([id_dobavljaca], [id_kategorije])" + " " +
                        $"SELECT {id_dobavljaca}, [Id]" + " " +
                        "FROM [Kategorija]" + " " +
                        "WHERE [naziv] IN (";
                for (int i = 0; i < kategorije.Count; i++)
                    update_query += $"'{kategorije[i]}'" + (i < kategorije.Count - 1 ? ", " : ")");

                BazaPodataka.UpdateOrDeleteQuery(update_query);
            }
        }
    }
}
