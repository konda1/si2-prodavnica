﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for SlabaProdajaControl.xaml
    /// </summary>
    public partial class SlabaProdajaControl : UserControlBase
    {
        string tablename;
        public SlabaProdajaControl(string title = null) : base(title)
        {
            InitializeComponent();
            NarucivanjeGlobals.slabaProdajaControl = this;
            RefreshDataGrid();
        }

        private class Proizvod
        {
            public int Id { get; set; }
            public string Kategorija { get; set; }
            public int Sifra { get; set; }
            public string Proizvodjac { get; set; }
            public string Ime { get; set; }
            public float Cena { get; set; }
            public int Stanje { get; set; }
            public float Garancija { get; set; }
            public string DatumPoslednjeKupovine { get; set; }
            public int BrojProdatih { get; set; }

            public static List<Proizvod> ParseReader(SqlDataReader reader)
            {
                List<Proizvod> proizvodi = new List<Proizvod>();
                while(reader.Read())
                {
                    proizvodi.Add(new Proizvod
                    {
                        Id = Convert.ToInt32(reader["Id"].ToString()),
                        Kategorija = reader["kategorija"].ToString(),
                        Sifra = Convert.ToInt32(reader["sifra"].ToString()),
                        Proizvodjac = reader["proizvodjac"].ToString(),
                        Ime = reader["ime"].ToString(),
                        Cena = float.Parse(reader["cena"].ToString(), CultureInfo.InvariantCulture.NumberFormat),
                        Stanje = Convert.ToInt32(reader["stanje"].ToString()),
                        Garancija = float.Parse(reader["garancija"].ToString(), CultureInfo.InvariantCulture.NumberFormat),
                        DatumPoslednjeKupovine = Convert.ToDateTime(reader["datum_poslednje_kupovine"]).ToString("dd.MM.yyyy"),
                        BrojProdatih = Convert.ToInt32(reader["broj_prodatih"].ToString()),
                    });
                }
                return proizvodi;
            }
        }

        public void RefreshDataGrid()
        {
            if (DataGrid == null)
                return;
            if (ComboBox.SelectedItem == null || !(Utils.StringInStringList(((ComboBoxItem)ComboBox.SelectedItem).Content.ToString(), Statistika.NadjiKategorijeSlaboProdavanihProizvoda()) || "Sve kategorije".CompareTo(((ComboBoxItem)ComboBox.SelectedItem).Content.ToString()) == 0))
            {
                ComboBox.SelectedItem = null;
                DataGrid.ItemsSource = null;
            }
            else
            {
                List<Proizvod> proizvodi = Proizvod.ParseReader(Statistika.NadjiSlaboProdavaneProizvode());
                if ("Sve kategorije".CompareTo(tablename) == 0)
                    DataGrid.ItemsSource = proizvodi;
                else
                    DataGrid.ItemsSource = proizvodi.Where(x => x.Kategorija == tablename);
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0) // Caused by Clear()
            {
                return;
            }
            ComboBoxItem item = (ComboBoxItem)ComboBox.SelectedItem;
            tablename = item.Content.ToString();

            try
            {
                RefreshDataGrid();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Neuspesno");
                Console.WriteLine(ex);
            }
        }

        private void ComboBox_DropDownOpened(object sender, EventArgs e)
        {
            ComboBox.Items.Clear();
            ComboBox.Items.Add(new ComboBoxItem
            {
                Content = "Sve kategorije"
            });
            foreach (string kategorija in Statistika.NadjiKategorijeSlaboProdavanihProizvoda())
            {
                ComboBox.Items.Add(new ComboBoxItem
                {
                    Content = kategorija
                });
            }
        }
    }
}
