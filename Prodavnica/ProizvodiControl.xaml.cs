﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for ProizvodiControl.xaml
    /// </summary>
    public partial class ProizvodiControl : UserControlBase
    {
        public ProizvodiControl(string title = null) : base(title)
        {
            InitializeComponent();
            UpdateActionTable();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (labelSlikaAdapter.Content.Equals(""))
            {
                labelSlikaAdapter.Visibility = Visibility.Hidden;
                labelSlikaAdapter.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[Adapter]", Convert.ToInt32(adapterSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Adapter] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [snagaW], [izlazniNapon], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', {7}, '{8}', '{9}', GETDATE())", adapterProizvodjac.Text, adapterModel.Text, Convert.ToInt32(adapterSifra.Text), Convert.ToDouble(adapterCena.Text), Convert.ToInt32(adapterStanje.Text), Convert.ToDouble(adapterGarancija.Text), adapterLink.Text, Convert.ToInt32(adapterSnagaW.Text), adapterIzlazniNapon.Text, "pack://siteoforigin:,,,/img/"+labelSlikaAdapter.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (labelSlikaEkterniDisk.Content.Equals(""))
            {
                labelSlikaEkterniDisk.Visibility = Visibility.Hidden;
                labelSlikaEkterniDisk.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[EksterniDisk]", Convert.ToInt32(eksterniDiskSifra1.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[EksterniDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [format], [kapacitet], [masa], [dimenzije], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', {7}, '{8}', {9}, '{10}', '{11}', '{12}', GETDATE())", esterniDiskProizvodjac1.Text, eksterniDiskModel1.Text, Convert.ToInt32(eksterniDiskSifra1.Text), Convert.ToDouble(eskterniDiskCena1.Text), Convert.ToInt32(eksterniDiskStanje1.Text), Convert.ToDouble(eksterniDiskGarancija1.Text), eksterniDiskLink1.Text, Convert.ToDouble(eksterniDiskFormat1.Text), eksterniDiskKapacitet.Text, Convert.ToDouble(eksterniDiskMasa1.Text), eksterniDiskDimenzije.Text, eksterniDiskPovezivanje.Text, "pack://siteoforigin:,,,/img/" + labelSlikaEkterniDisk.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            if (labelSlikaHardDisk.Content.Equals(""))
            {
                labelSlikaHardDisk.Visibility = Visibility.Hidden;
                labelSlikaHardDisk.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[HardDisk]", Convert.ToInt32(hardDiskSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[HardDisk] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [format], [povezivanje], [kapacitet], [bafer], [obrtaji], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', {9}, '{10}', '{11}', '{12}', '{13}', GETDATE())", hardDiskProizvodjac.Text, hardDiskModel.Text, Convert.ToInt32(hardDiskSifra.Text), Convert.ToDouble(hardDiskCena.Text), Convert.ToInt32(hardDiskStanje.Text), Convert.ToDouble(hardDiskGarancija.Text), hardDiskLink.Text, hardDiskTip.Text, Convert.ToDouble(hardDiskFormat.Text), hardDiskPovezivanje.Text, hardDiskKapacitet.Text, hardDiskBafer.Text, hardDiskObrtaji.Text, "pack://siteoforigin:,,,/img/" + labelSlikaHardDisk.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            if (labelSlikaKabl.Content.Equals(""))
            {
                labelSlikaKabl.Visibility = Visibility.Hidden;
                labelSlikaKabl.Content = "Nijedna slika nije odabrana.jpg";
            } 
            if (BazaPodataka.ProveraSifre("[dbo].[Kabl]", Convert.ToInt32(kablSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Kabl] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [strana1], [strana2], [duzina], [napomena], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', {9}, '{10}', '{11}', GETDATE())", kablProizvodjac.Text, kablModel.Text, Convert.ToInt32(kablSifra.Text), Convert.ToDouble(kablCena.Text), Convert.ToInt32(kablStanje.Text), Convert.ToDouble(kablGarancija.Text), kablLink.Text, kablStrana1.Text, kablStrana2.Text, kablDuzina.Text, kablNapomena.Text, "pack://siteoforigin:,,,/img/" + labelSlikaKabl.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            if (labelSlikaMikrofon.Content.Equals(""))
            {
                labelSlikaMikrofon.Visibility = Visibility.Hidden;
                labelSlikaMikrofon.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[Mikrofon]", Convert.ToInt32(mikrofonSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Mikrofon] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [duzina], [frekvencija], [boja], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', {7}, '{8}', {9}, '{10}', '{11}', GETDATE())", mikrofonProizvodjac.Text, mikrofonModel.Text, Convert.ToInt32(mikrofonSifra.Text), Convert.ToDouble(mikrofonCena.Text), Convert.ToInt32(mikrofonStanje.Text), Convert.ToDouble(mikrofonGarancija.Text), mikrofonLink.Text, Convert.ToInt32(mikrofonDuzina.Text), mikrofonFrekvencija.Text, mikrofonBoja.Text, mikrofonPovezivanje.Text, "pack://siteoforigin:,,,/img/" + labelSlikaMikrofon.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            if (labelSlikaMis.Content.Equals(""))
            {
                labelSlikaMis.Visibility = Visibility.Hidden;
                labelSlikaMis.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[Mis]", Convert.ToInt32(misSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Mis] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', GETDATE())", misProizvodjac.Text, misModel.Text, Convert.ToInt32(misSifra.Text), Convert.ToDouble(misCena.Text), Convert.ToInt32(misStanje.Text), Convert.ToDouble(misGarancija.Text), misLink.Text, misTip.Text, "pack://siteoforigin:,,,/img/" + labelSlikaMis.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            if (labelSlikaMonitor.Content.Equals(""))
            {
                labelSlikaMonitor.Visibility = Visibility.Hidden;
                labelSlikaMonitor.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[Monitor]", Convert.ToInt32(monitorSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Monitor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [refreshRate], [rezolucija], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', {8}, {9}, '{10}', '{11}', GETDATE())", monitorProizvodjac.Text, monitorModel.Text, Convert.ToInt32(monitorSifra.Text), Convert.ToDouble(monitorCena.Text), Convert.ToInt32(monitorStanje.Text), Convert.ToDouble(monitorGarancija.Text), monitorLink.Text, monitorTip.Text, Convert.ToInt32(monitorVelicina.Text), Convert.ToInt32(monitorRefreshRate.Text), monitorRezolucija.Text, "pack://siteoforigin:,,,/img/" + labelSlikaMonitor.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            if (labelSlikaPodloga.Content.Equals(""))
            {
                labelSlikaPodloga.Visibility = Visibility.Hidden;
                labelSlikaPodloga.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[Podloga]", Convert.ToInt32(podlogaSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Podloga] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', '{9}', GETDATE())", podlogaProizvodjac.Text, podlogaModel.Text, Convert.ToInt32(podlogaSifra.Text), Convert.ToDouble(podlogaCena.Text), Convert.ToInt32(podlogaStanje.Text), Convert.ToDouble(podlogaGarancija.Text), podlogaLink.Text, podlogaTip.Text, podlogaVelicina.Text, "pack://siteoforigin:,,,/img/" + labelSlikaPodloga.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            if (labelSlikaProjektor.Content.Equals(""))
            {
                labelSlikaProjektor.Visibility = Visibility.Hidden;
                labelSlikaProjektor.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[Projektor]", Convert.ToInt32(projektorSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Projektor] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [rezolucija], [osvetljenje], [kontrast], [povezivanje], [zvucniciSnaga], [zvucniciTip], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', {13}, '{14}', '{15}', GETDATE())", projektorProizvodjac.Text, projektorModel.Text, Convert.ToInt32(projektorSifra.Text), Convert.ToDouble(projektorCena.Text), Convert.ToInt32(projektorStanje.Text), Convert.ToDouble(projektorGarancija.Text), projektorLink.Text, projektorTip.Text, projektorVelicina.Text, projektorRezolucija.Text, projektorOsvetljenje.Text, projektorKontrast.Text, projektorPovezivanje.Text, Convert.ToInt32(projektorZvucniciSnaga.Text), projektorZvucniciTip.Text, "pack://siteoforigin:,,,/img/" + labelSlikaProjektor.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            if (labelSlikaSkener.Content.Equals(""))
            {
                labelSlikaSkener.Visibility = Visibility.Hidden;
                labelSlikaSkener.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[Skener]", Convert.ToInt32(skenerSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Skener] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', '{9}', GETDATE())", skenerProizvodjac.Text, skenerModel.Text, Convert.ToInt32(skenerSifra.Text), Convert.ToDouble(skenerCena.Text), Convert.ToInt32(skenerStanje.Text), Convert.ToDouble(skenerGarancija.Text), skenerLink.Text, skenerTip.Text, skenerVelicina.Text, "pack://siteoforigin:,,,/img/" + labelSlikaSkener.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_13(object sender, RoutedEventArgs e)
        {
            if (labelSlikaSlusalice.Content.Equals(""))
            {
                labelSlikaSlusalice.Visibility = Visibility.Hidden;
                labelSlikaSlusalice.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[Slusalice]", Convert.ToInt32(slusaliceSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Slusalice] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [duzina], [frekvencija], [boja], [mikrofon], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', {8}, '{9}', '{10}', '{11}', '{12}', GETDATE())", slusaliceProizvodjac.Text, slusaliceModel.Text, Convert.ToInt32(slusaliceSifra.Text), Convert.ToDouble(slusaliceCena.Text), Convert.ToInt32(slusaliceStanje.Text), Convert.ToDouble(slusaliceGarancija.Text), slusaliceLink.Text, slusaliceTip.Text, Convert.ToDouble(slusaliceDuzina.Text), slusaliceFrekvencija.Text, slusaliceBoja.Text, slusaliceMikrofon.Text, "pack://siteoforigin:,,,/img/" + labelSlikaSlusalice.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_14(object sender, RoutedEventArgs e)
        {
            if (labelSlikaStampac.Content.Equals(""))
            {
                labelSlikaStampac.Visibility = Visibility.Hidden;
                labelSlikaStampac.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[Stampac]", Convert.ToInt32(stampacSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Stampac] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', GETDATE())", stampacProizvodjac.Text, stampacModel.Text, Convert.ToInt32(stampacSifra.Text), Convert.ToDouble(stampacCena.Text), Convert.ToInt32(stampacStanje.Text), Convert.ToDouble(stampacGarancija.Text), stampacLink.Text, stampacTip.Text, "pack://siteoforigin:,,,/img/" + labelSlikaStampac.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_15(object sender, RoutedEventArgs e)
        {
            if (labelSlikaTastatura.Content.Equals(""))
            {
                labelSlikaTastatura.Visibility = Visibility.Hidden;
                labelSlikaTastatura.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[Tastatura]", Convert.ToInt32(tastaturaSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Tastatura] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', GETDATE())", tastaturaProizvodjac.Text, tastaturaModel.Text, Convert.ToInt32(tastaturaSifra.Text), Convert.ToDouble(tastaturaCena.Text), Convert.ToInt32(tastaturaStanje.Text), Convert.ToDouble(tastaturaGarancija.Text), tastaturaLink.Text, tastaturaTip.Text, "pack://siteoforigin:,,,/img/" + labelSlikaTastatura.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_16(object sender, RoutedEventArgs e)
        {
            if (labelSlikaUSB.Content.Equals(""))
            {
                labelSlikaUSB.Visibility = Visibility.Hidden;
                labelSlikaUSB.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[USBMemorija]", Convert.ToInt32(usbSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[USBMemorija] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [ulaz], [kapacitet], [boja], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', '{9}', '{10}', GETDATE())", usbProizvodjac.Text, usbModel.Text, Convert.ToInt32(usbSifra.Text), Convert.ToDouble(usbCena.Text), Convert.ToInt32(usbStanje.Text), Convert.ToDouble(usbGarancija.Text), usbLink.Text, usbUlaz.Text, usbKapacitet.Text, usbBoja.Text, "pack://siteoforigin:,,,/img/" + labelSlikaUSB.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void Button_Click_17(object sender, RoutedEventArgs e)
        {
            if (labelSlikaZvucnik.Content.Equals(""))
            {
                labelSlikaZvucnik.Visibility = Visibility.Hidden;
                labelSlikaZvucnik.Content = "Nijedna slika nije odabrana.jpg";
            }
            if (BazaPodataka.ProveraSifre("[dbo].[Zvucnik]", Convert.ToInt32(zvucnikSifra.Text)))
                BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Zvucnik] ([proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link], [tip], [velicina], [sistem], [izlaznaSnaga], [frekvencija], [ulazSlusalice], [povezivanje], [path], [datum_poslednje_kupovine]) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', '{9}', {10}, '{11}', '{12}', '{13}', '{14}', GETDATE())", zvucnikProizvodjac.Text, zvucnikModel.Text, Convert.ToInt32(zvucnikSifra.Text), Convert.ToDouble(zvucnikCena.Text), Convert.ToInt32(zvucnikStanje.Text), Convert.ToDouble(zvucnikGarancija.Text), zvucnikLink.Text, zvucnikTip.Text, zvucnikVelicina.Text, zvucnikSistem.Text, Convert.ToDouble(zvucnikIzlaznaSnaga.Text), zvucnikFrekvencija.Text, zvucnikUlazSlusalice.Text, zvucnikPovezivanje.Text, "pack://siteoforigin:,,,/img/" + labelSlikaZvucnik.Content));
            else
                MessageBox.Show("Neuspesno, proverite sifru proizvoda!");
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridIzmeni.ItemsSource = null;

            ComboBoxItem item = (ComboBoxItem)ComboBoxProizvod.SelectedItem;
            string tablename = item.Content.ToString();
            string query = String.Format("SELECT [id], [proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link] FROM [dbo].[{0}]", tablename);
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    DataTable dt = new DataTable(tablename);
                    sda.Fill(dt);
                    ///DataGrid.ItemsSource = dt.DefaultView;
                    //sda.Update(dt);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    DataGridProizvod.ItemsSource = dt.DefaultView;
                }
                catch (Exception ex)
                {
                   // MessageBox.Show("Neuspesno");
                    Console.WriteLine(ex);
                }

            }

        }

        private void UpdateProductTable(string tablename)
        {
            DataGridIzmeni.ItemsSource = null;
            string query = String.Format("SELECT [id], [proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], [link] FROM [dbo].[{0}]", tablename);
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    DataTable dt = new DataTable("Proizvodi");
                    sda.Fill(dt);
                    ///DataGrid.ItemsSource = dt.DefaultView;
                    sda.Update(dt);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    dt.AcceptChanges();
                    DataGridProizvod.ItemsSource = dt.DefaultView;
                }
                catch (Exception ex)
                {
                    //   MessageBox.Show("Neuspesno");
                    Console.WriteLine(ex);
                }
            }
        }

        public void BtnObrisiProizvod(object sender, RoutedEventArgs e)
        {
            try
            {
                ComboBoxItem item = (ComboBoxItem)ComboBoxProizvod.SelectedItem;
                string tablename = item.Content.ToString();
                //DataGridProizvod.ItemsSource = null;
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                int id = Int32.Parse(dataRowView[0].ToString());
                String query = String.Format("DELETE FROM [dbo].[{0}] Where [Id] = {1}", tablename, id);
                BazaPodataka.InsertionIntoDB(query);
                UpdateProductTable(tablename);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void DataGridProizvod_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ComboBoxItem item = (ComboBoxItem)ComboBoxProizvod.SelectedItem;
            string tablename = item.Content.ToString();

            DataGrid dataGrid = sender as DataGrid;
            DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
            DataGridCell RowColumn = dataGrid.Columns[0].GetCellContent(row).Parent as DataGridCell;
            string CellValue = ((TextBlock)RowColumn.Content).Text;
            int id = Int32.Parse(CellValue);

            string query = String.Format("SELECT * FROM [dbo].[{0}] WHERE [id] = {1}", tablename, id);

            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    DataTable dt = new DataTable(tablename);
                    sda.Fill(dt);
                    ///DataGrid.ItemsSource = dt.DefaultView;
                    //sda.Update(dt);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    DataGridIzmeni.ItemsSource = dt.DefaultView;
                }
                catch (Exception ex)
                {
                   // MessageBox.Show("Neuspesno");
                    Console.WriteLine(ex);
                }

            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ComboBoxItem item = (ComboBoxItem)ComboBoxProizvod.SelectedItem;
            string tablename = item.Content.ToString();

            DataGrid dataGrid = DataGridIzmeni;
            if(dataGrid.SelectedIndex != -1) {
                DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
                DataGridCell RowColumn = dataGrid.Columns[0].GetCellContent(row).Parent as DataGridCell;
                string CellValue = ((TextBlock)RowColumn.Content).Text;
                int id = Int32.Parse(CellValue);

                string query = String.Format("SELECT * FROM [dbo].[{0}] WHERE [id] = {1}", tablename, id);
                using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand(query, conn);
                    command.ExecuteNonQuery();
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    try
                    {

                        DataSet ds = new DataSet("Izmena");
                        sda.Fill(ds);
                        ///DataGrid.ItemsSource = dt.DefaultView;
                        for (int i = 1; i < ds.Tables[0].Columns.Count; i++)
                        {
                            DataGridCell RowColumn1 = dataGrid.Columns[i].GetCellContent(row).Parent as DataGridCell;
                            string CellValue1 = ((TextBlock)RowColumn1.Content).Text;
                            Type t = ds.Tables[0].Columns[i].DataType;
                            if (t == typeof(int))
                            {
                                if (CellValue1.Trim().Equals(""))
                                    ds.Tables[0].Rows[0][i] = DBNull.Value;
                                else
                                    ds.Tables[0].Rows[0][i] = Int32.Parse(CellValue1);
                            }
                            else if (t == typeof(Single))
                            {
                                if (CellValue1.Trim().Equals(""))
                                    ds.Tables[0].Rows[0][i] = DBNull.Value;
                                else
                                    ds.Tables[0].Rows[0][i] = Single.Parse(CellValue1, new System.Globalization.CultureInfo("us-US"));
                            }
                            else
                            {
                                if (CellValue1.Trim().Equals(""))
                                    ds.Tables[0].Rows[0][i] = DBNull.Value;
                                else
                                    ds.Tables[0].Rows[0][i] = CellValue1;
                            }
                        }

                        SqlCommandBuilder cb = new SqlCommandBuilder(sda);
                        sda.UpdateCommand = cb.GetUpdateCommand();
                        sda.Update(ds);
                        command.Dispose();
                        sda.Dispose();
                        conn.Close();
                        UpdateProductTable(tablename);

                        DataGridIzmeni.ItemsSource = null;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }
            else
            {
                MessageBox.Show("Odaberite proizvod!");
            }
            

            

        }

        string[] tables = { "Adapter", "EksterniDisk", "HardDisk", "Kabl", "Mikrofon", "Mis", "Monitor", "Podloga", "Projektor", "Skener", "Slusalice", "Stampac", "Tastatura", "USBMemorija", "Zvucnik" };

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            byte popust;
            int sifrap;
            int promenac;

            if (sifra.Text.Equals(""))
                sifrap = 0;
            else
                sifrap = Int32.Parse(sifra.Text);

            promenac = Int32.Parse(promena.Text);

            double prom = 0;
            if (radioPoskuljenje.IsChecked.Value)
            {
                prom = (100 + (double)promenac) / 100;
                popust = 0;
            }
            else
            {
                prom = (100 - (double)promenac) / 100;
                popust = 1;
            }

            BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[Akcija] VALUES ('{0}', '{1}', {2}, {3}, {4})", tip.Text, proizvodjac.Text, sifrap, promenac, popust));
            
            try
            {
                if (!radioSnizenje.IsChecked.Value && !radioPoskuljenje.IsChecked.Value)
                    throw new Exception("Odaberite da li je snizenje ili poskupljenje");  

                string query;
                    
                if(tip.Text.Equals(""))
                {
                    for(int i = 0; i < tables.Length; i++)
                    {
                        if (proizvodjac.Text.Equals("") && sifrap == 0)
                        {
                            query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2)", tables[i], prom.ToString(new System.Globalization.CultureInfo("us-US")));
                        }
                        else if (!proizvodjac.Text.Equals("") && sifrap == 0)
                        {
                            query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [proizvodjac] = '{2}'", tables[i], prom.ToString(new System.Globalization.CultureInfo("us-US")), proizvodjac.Text);
                        }
                        else if (proizvodjac.Text.Equals("") && sifrap != 0)
                        {
                            query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [sifra] = {2}", tables[i], prom.ToString(new System.Globalization.CultureInfo("us-US")), sifrap);
                        }
                        else
                        {
                            query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [proizvodjac] = '{2}' AND [sifra] = {3}", tables[i], prom.ToString(new System.Globalization.CultureInfo("us-US")), proizvodjac.Text, sifrap);
                        }
                        BazaPodataka.UpdateDB(query);
                    }
                }
                else
                {
                    if (proizvodjac.Text.Equals("") && sifrap == 0)
                    {
                        query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2)", tip.Text, prom.ToString(new System.Globalization.CultureInfo("us-US")));
                    }
                    else if (!proizvodjac.Text.Equals("") && sifrap == 0)
                    {
                        query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [proizvodjac] = '{2}'", tip.Text, prom.ToString(new System.Globalization.CultureInfo("us-US")), proizvodjac.Text);
                    }
                    else if (proizvodjac.Text.Equals("") && sifrap != 0)
                    {
                        query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [sifra] = {2}", tip.Text, prom.ToString(new System.Globalization.CultureInfo("us-US")), sifrap);
                    }
                    else
                    {
                        query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [proizvodjac] = '{2}' AND [sifra] = {3}", tip.Text, prom.ToString(new System.Globalization.CultureInfo("us-US")), proizvodjac.Text, sifrap);
                    }
                    BazaPodataka.UpdateDB(query);
                }
                UpdateActionTable();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                Console.WriteLine(ex);
            }
        }
        
        private void Ponisti_akciju(object sender, RoutedEventArgs e)
        {

            DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
            int id = Int32.Parse(dataRowView[0].ToString());
            string tip = dataRowView[1].ToString();
            string proizvodjac = dataRowView[2].ToString();
            int sifra = Int32.Parse(dataRowView[3].ToString());
            int promena = Int32.Parse(dataRowView[4].ToString());
            bool popust = Boolean.Parse(dataRowView[5].ToString());

            double prom;
            if (popust == true)
                prom = 100 / (100 - (double)promena);
            else
                prom = 100 / (100 + (double)promena);

            try
            {
                string query;

                if (tip.Equals(""))
                {
                    for (int i = 0; i < tables.Length; i++)
                    {
                        if (proizvodjac.Equals("") && sifra == 0)
                        {
                            query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2)", tables[i], prom.ToString(new System.Globalization.CultureInfo("us-US")));
                        }
                        else if (!proizvodjac.Equals("") && sifra == 0)
                        {
                            query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [proizvodjac] = '{2}'", tables[i], prom.ToString(new System.Globalization.CultureInfo("us-US")), proizvodjac);
                        }
                        else if (proizvodjac.Equals("") && sifra != 0)
                        {
                            query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [sifra] = {2}", tables[i], prom.ToString(new System.Globalization.CultureInfo("us-US")), sifra);
                        }
                        else
                        {
                            query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [proizvodjac] = '{2}' AND [sifra] = {3}", tables[i], prom.ToString(new System.Globalization.CultureInfo("us-US")), proizvodjac, sifra);
                        }
                        BazaPodataka.UpdateDB(query);
                    }
                }
                else
                {
                    if (proizvodjac.Equals("") && sifra == 0)
                    {
                        query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2)", tip, prom.ToString(new System.Globalization.CultureInfo("us-US")));
                    }
                    else if (!proizvodjac.Equals("") && sifra == 0)
                    {
                        query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [proizvodjac] = '{2}'", tip, prom.ToString(new System.Globalization.CultureInfo("us-US")), proizvodjac);
                    }
                    else if (proizvodjac.Equals("") && sifra != 0)
                    {
                        query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [sifra] = {2}", tip, prom.ToString(new System.Globalization.CultureInfo("us-US")), sifra);
                    }
                    else
                    {
                        query = String.Format("UPDATE [dbo].[{0}] SET [cena] = ROUND([cena]*{1},2) WHERE [proizvodjac] = '{2}' AND [sifra] = {3}", tip, prom.ToString(new System.Globalization.CultureInfo("us-US")), proizvodjac, sifra);
                    }
                    BazaPodataka.UpdateDB(query);
                }
                BazaPodataka.DeleteFromDB(String.Format("DELETE FROM [dbo].[Akcija] WHERE [Id] = {0}", id));
                UpdateActionTable();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                Console.WriteLine(ex);
            }
        }

        private void UpdateActionTable()
        {
            string query = String.Format("SELECT * FROM [dbo].[Akcija]");
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    DataTable dt = new DataTable("Akcija");
                    sda.Fill(dt);
                    ///DataGrid.ItemsSource = dt.DefaultView;
                    sda.Update(dt);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    DataGridAkcije.ItemsSource = dt.DefaultView;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Neuspesno");
                    Console.WriteLine(ex);
                }
            }
        }

        private void DataGridIzmeni_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyType == typeof(DateTime))
            {
                DataGridTextColumn dataGridTextColumn = e.Column as DataGridTextColumn;
                if (dataGridTextColumn != null)
                {
                    dataGridTextColumn.Binding.StringFormat = "{0:d.M.yyyy.}";
                }
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaAdapter.Visibility = Visibility.Visible;
                    labelSlikaAdapter.Content = filename1;
                    break;
                default:
                    labelSlikaAdapter.Visibility = Visibility.Hidden;
                    labelSlikaAdapter.Content = "";
                    break;
            }
        }

        private void Button_Click_18(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaEkterniDisk.Visibility = Visibility.Visible;
                    labelSlikaEkterniDisk.Content = filename1;
                    break;
                default:
                    labelSlikaEkterniDisk.Visibility = Visibility.Hidden;
                    labelSlikaEkterniDisk.Content = "";
                    break;
            }
        }

        private void Button_Click_19(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaHardDisk.Visibility = Visibility.Visible;
                    labelSlikaHardDisk.Content = filename1;
                    break;
                default:
                    labelSlikaHardDisk.Visibility = Visibility.Hidden;
                    labelSlikaHardDisk.Content = "";
                    break;
            }
        }

        private void Button_Click_20(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaKabl.Visibility = Visibility.Visible;
                    labelSlikaKabl.Content = filename1;
                    break;
                default:
                    labelSlikaKabl.Visibility = Visibility.Hidden;
                    labelSlikaKabl.Content = "";
                    break;
            }
        }

        private void Button_Click_21(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaMis.Visibility = Visibility.Visible;
                    labelSlikaMis.Content = filename1;
                    break;
                default:
                    labelSlikaMis.Visibility = Visibility.Hidden;
                    labelSlikaMis.Content = "";
                    break;
            }
        }

        private void Button_Click_22(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaMikrofon.Visibility = Visibility.Visible;
                    labelSlikaMikrofon.Content = filename1;
                    break;
                default:
                    labelSlikaMikrofon.Visibility = Visibility.Hidden;
                    labelSlikaMikrofon.Content = "";
                    break;
            }
        }

        private void Button_Click_23(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaMonitor.Visibility = Visibility.Visible;
                    labelSlikaMonitor.Content = filename1;
                    break;
                default:
                    labelSlikaMonitor.Visibility = Visibility.Hidden;
                    labelSlikaMonitor.Content = "";
                    break;
            }
        }

        private void Button_Click_24(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaPodloga.Visibility = Visibility.Visible;
                    labelSlikaPodloga.Content = filename1;
                    break;
                default:
                    labelSlikaPodloga.Visibility = Visibility.Hidden;
                    labelSlikaPodloga.Content = "";
                    break;
            }
        }

        private void Button_Click_25(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();

            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaProjektor.Visibility = Visibility.Visible;
                    labelSlikaProjektor.Content = filename1;
                    break;
                default:
                    labelSlikaProjektor.Visibility = Visibility.Hidden;
                    labelSlikaProjektor.Content = "";
                    break;
            }
        }

        private void Button_Click_26(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();

            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaSkener.Visibility = Visibility.Visible;
                    labelSlikaSkener.Content = filename1;
                    break;
                default:
                    labelSlikaSkener.Visibility = Visibility.Hidden;
                    labelSlikaSkener.Content = "";
                    break;
            }
        }

        private void Button_Click_27(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();

            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaSlusalice.Visibility = Visibility.Visible;
                    labelSlikaSlusalice.Content = filename1;
                    break;
                default:
                    labelSlikaSlusalice.Visibility = Visibility.Hidden;
                    labelSlikaSlusalice.Content = "";
                    break;
            }
        }

        private void Button_Click_28(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();

            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaStampac.Visibility = Visibility.Visible;
                    labelSlikaStampac.Content = filename1;
                    break;
                default:
                    labelSlikaStampac.Visibility = Visibility.Hidden;
                    labelSlikaStampac.Content = "";
                    break;
            }
        }

        private void Button_Click_29(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();

            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaTastatura.Visibility = Visibility.Visible;
                    labelSlikaTastatura.Content = filename1;
                    break;
                default:
                    labelSlikaTastatura.Visibility = Visibility.Hidden;
                    labelSlikaTastatura.Content = "";
                    break;
            }
        }

        private void Button_Click_30(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();

            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaUSB.Visibility = Visibility.Visible;
                    labelSlikaUSB.Content = filename1;
                    break;
                default:
                    labelSlikaUSB.Visibility = Visibility.Hidden;
                    labelSlikaUSB.Content = "";
                    break;
            }
        }

        private void Button_Click_31(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";


            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();

            switch (result)
            {
                case true:
                    string filename = dlg.FileName;
                    string filename1 = dlg.SafeFileName;
                    File.Move(filename, "../../bin/Debug/img/" + filename1);
                    labelSlikaZvucnik.Visibility = Visibility.Visible;
                    labelSlikaZvucnik.Content = filename1;
                    break;
                default:
                    labelSlikaZvucnik.Visibility = Visibility.Hidden;
                    labelSlikaZvucnik.Content = "";
                    break;
            }
        }

        private void DataGridIzmeni_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}