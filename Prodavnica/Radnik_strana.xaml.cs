﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for Radnik_strana.xaml
    /// </summary>
    public partial class Radnik_strana : Window
    {

        DataTable tabelaRacun = new DataTable("Račun");
        string tablename;
        private int i = 0;
        private double ukupnaCena = 0;
        public Radnik_strana()
        {
            InitializeComponent();
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
            tabelaRacun.Columns.Add("Proizvod", typeof(String));
            tabelaRacun.Columns.Add("Šifra proizvoda", typeof(String));
            tabelaRacun.Columns.Add("Proizvođač", typeof(String));
            tabelaRacun.Columns.Add("Ime proizvoda", typeof(String));
            tabelaRacun.Columns.Add("Cena", typeof(String));
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem item = (ComboBoxItem) ComboBox.SelectedItem;
            tablename = item.Content.ToString();
            string query = String.Format("SELECT * FROM {0} WHERE [stanje] > 0", tablename);
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    DataTable dt = new DataTable(tablename);
                    sda.Fill(dt);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    DataGrid.ItemsSource = dt.DefaultView;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Neuspesno");
                    Console.WriteLine(ex);
                }
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BtnKupi(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                double cena = Math.Round((1.21212121*Double.Parse(dataRowView[4].ToString())), 2);
                DataRow racunRed = tabelaRacun.NewRow();
                racunRed[0] = tablename;
                racunRed[1] = dataRowView[3].ToString();
                racunRed[2] = dataRowView[1].ToString();
                racunRed[3] = dataRowView[2].ToString();
                racunRed[4] = cena.ToString();
                tabelaRacun.Rows.Add(racunRed);
                DataGridRacun.ItemsSource = tabelaRacun.DefaultView;
                ukupnaCena += cena;
                textBox.Text = "Vaš račun: "+(Math.Round(ukupnaCena,2)).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DataGrid_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BtnKorpa(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                DataGridRacun.ItemsSource = null;
                tabelaRacun.AcceptChanges();
                foreach (DataRow row in tabelaRacun.Rows)
                {
                    if (row[1].Equals(dataRowView[1]))
                    {
                        ukupnaCena -= Double.Parse(row[4].ToString());
                        row.Delete();
                    }
                }
                tabelaRacun.AcceptChanges();
                DataGridRacun.ItemsSource = tabelaRacun.DefaultView;
                textBox.Text = "Vaš račun: " + (Math.Round(ukupnaCena, 2)).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }

        private void PotvrdiKupovinu_Click(object sender, RoutedEventArgs e)
        {
            List<String> query = new List<String>();
            var time = DateTime.Now;
            String racun = "racun"+time.ToString("dd_MM_yyyy-hh_mm");
            String path = String.Format("../../Racuni/{0}.pdf", racun);
            DataRow racunRed = tabelaRacun.NewRow();
            racunRed[0] = "";
            racunRed[1] = "";
            racunRed[2] = "";
            racunRed[3] = "";
            racunRed[4] = (Math.Round(ukupnaCena, 2)).ToString();
            tabelaRacun.Rows.Add(racunRed);
            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(@path, FileMode.Create));
            doc.Open();
            PdfPTable table = new PdfPTable(tabelaRacun.Columns.Count);
            table.WidthPercentage = 100;
            for(int i = 0; i < tabelaRacun.Columns.Count; i++)
            {
                PdfPCell cell = new PdfPCell(new Phrase(tabelaRacun.Columns[i].ColumnName));
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                cell.BackgroundColor = new iTextSharp.text.BaseColor(51, 102, 102);

                table.AddCell(cell);
            }

            for (int i = 0; i < tabelaRacun.Rows.Count; i++)
            {
                for (int j = 0; j < tabelaRacun.Columns.Count; j++)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(tabelaRacun.Rows[i][j].ToString()));

                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;

                    table.AddCell(cell);
                }
            }
            doc.Add(table);
            doc.Close();
            for(int k = 0; k < tabelaRacun.Rows.Count - 1; k++)
                query.Add(String.Format("UPDATE [dbo].[{0}] SET [stanje]=[stanje]-1 WHERE [sifra]='{1}'", tabelaRacun.Rows[i][0].ToString(), tabelaRacun.Rows[i][1].ToString()));
  
            for (int i = 0; i < query.Count; i++)
                BazaPodataka.InsertionIntoDB(query[i]);
            
            tabelaRacun.AcceptChanges();
            foreach (DataRow row in tabelaRacun.Rows)
                row.Delete();            
            tabelaRacun.AcceptChanges();
            ukupnaCena = 0;
            textBox.Text = "Vaš račun: ";

        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
