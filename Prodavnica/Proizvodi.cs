﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prodavnica
{
    public static class Proizvodi
    {
        public static ReadOnlyCollection<string> Kategorije { get; private set; }
        static Proizvodi()
        {
            SqlDataReader reader = BazaPodataka.SelectQuery("SELECT [naziv] FROM [Kategorija]");
            Kategorije = new ReadOnlyCollection<string>(Utils.GetValuesListFromReader<string>(reader, "naziv"));
        }

        public static bool ValidacijaKategorije(string kategorija)
        {
            foreach (string k in Kategorije)
            {
                if (k.Equals(kategorija))
                    return true;
            }
            return false;
        }
        public static void EValidacijaKategorije(string kategorija)
        {
            if (!ValidacijaKategorije(kategorija))
                throw new ArgumentException(string.Format("Kategorija '{0}' ne postoji.", kategorija));
        }

        public static DataTable NadjiProizvode(string kategorija, string query)
        {
            EValidacijaKategorije(kategorija);

            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);

                DataTable dt = new DataTable(kategorija);
                sda.Fill(dt);
                command.Dispose();
                sda.Dispose();
                conn.Close();
                return dt;
            }
        }

        public static DataTable NadjiProizvodeNaStanju(string kategorija)
        {
            string query = string.Format("SELECT [id], [proizvodjac], [ime], [sifra], [cena], [stanje], [garancija], '{0}' AS Proizvod FROM {0} WHERE [stanje] > 0", kategorija);
            return NadjiProizvode(kategorija, query);
        }

        public static DataTable NadjiProizvodeBezStanja(string kategorija)
        {
            string query = string.Format("SELECT [id], [proizvodjac], [ime], [sifra], [cena], [stanje], [garancija] FROM {0} WHERE [stanje] <= 0", kategorija);
            return NadjiProizvode(kategorija, query);
        }

        public static DataTable NadjiNaruceneProizvode(string kategorija)
        {
            string query = string.Format(
                "SELECT [Porudzbina].[id] AS 'id_porudzbine', [{0}].[id] AS id, [proizvodjac], [{0}].[ime] AS ime, [{0}].[sifra] AS sifra, [{0}].[cena] AS cena, [{0}].[garancija] AS garancija, [Porudzbina].[kolicina] AS kolicina" + " " +
                "FROM [{0}], [NabavniProizvod], [Porudzbina]" + " " +
                "WHERE [NabavniProizvod].[tabela] = '{0}' AND" + " " +
                "[NabavniProizvod].[id_u_tabeli] = [{0}].id AND" + " " +
                "[NabavniProizvod].[Id] = [Porudzbina].[id_proizvoda] AND" + " " +
                "[Porudzbina].[kolicina] > 0",
                kategorija);
            return NadjiProizvode(kategorija, query);
        }

        public static bool NaruciProizvod(string kategorija, int id, int kolicina)
        {
            if(kolicina <= 0)
            {
                throw new ArgumentException("Kolicina mora biti veca od nule!");
            }
            NabavniProizvod np = NabavniProizvod.Nadji(kategorija, id);
            if (np != null)
            {
                using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
                {
                    conn.Open();

                    string query = @"SELECT * FROM [Porudzbina] WHERE id_proizvoda = @id_proizvoda";

                    using (SqlCommand command = new SqlCommand(query, conn))
                    {
                        command.Parameters.AddWithValue("@id_proizvoda", np.id);

                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            bool uspesno = BazaPodataka.UpdateDB(string.Format("UPDATE [dbo].[Porudzbina] SET kolicina = {0} WHERE id = {1}", Convert.ToInt32(reader["kolicina"].ToString()) + kolicina, reader["id"].ToString()));
                            return uspesno;
                        }
                    }
                }
            }
            return false;
        }

        public static List<string> KategorijeBezStanja()
        {
            List<string> kategorije = new List<string>();
            foreach (string tabela in Proizvodi.Kategorije)
            {
                string query = string.Format(
                    "SELECT COUNT(*) as count" + " " +
                    "FROM [{0}], [NabavniProizvod]" + " " +
                    "WHERE [NabavniProizvod].[tabela] = '{0}' AND" + " " +
                    "[NabavniProizvod].[id_u_tabeli] = [{0}].[id]" + " " +
                    "AND [{0}].[stanje] = 0",
                    tabela
                );
                int count = BazaPodataka.CountQuery(query, "count");
                if (count > 0)
                {
                    kategorije.Add(tabela);
                }
            }
            return kategorije;
        }

        public static List<string> NaruceneKategorije()
        {
            List<string> kategorije = new List<string>();
            string query =
                "SELECT DISTINCT [tabela] AS kategorija" + " " +
                "FROM [Porudzbina] JOIN [NabavniProizvod]" + " " +
                "ON [id_proizvoda] = [NabavniProizvod].[Id]" + " " +
                "WHERE [kolicina] > 0";
            for ( SqlDataReader reader = BazaPodataka.SelectQuery(query); reader.Read(); kategorije.Add(reader["kategorija"].ToString()) );
            return kategorije;
        }

        public static bool PrijemProizvoda(int id_porudzbine)
        {
            SqlDataReader reader = BazaPodataka.SelectQuery("SELECT [kolicina], [id_proizvoda] FROM [Porudzbina] WHERE [Id] = @id", new Dictionary<string, object> { { "@id", id_porudzbine } });
            if(reader.Read())
            {
                int kolicina = Convert.ToInt32(reader["kolicina"].ToString());
                int id_proizvoda = Convert.ToInt32(reader["id_proizvoda"].ToString());
                if(kolicina > 0)
                {
                    bool uspesno1 = BazaPodataka.UpdateOrDeleteQuery("UPDATE [Porudzbina] SET [kolicina] = [kolicina] - @kolicina WHERE [Id] = @id", new Dictionary<string, object>
                    {
                        { "@id", id_porudzbine },
                        { "@kolicina", kolicina },
                    }) > 0;
                    if(uspesno1)
                    {
                        NabavniProizvod np = NabavniProizvod.Nadji(id_proizvoda);
                        bool uspesno2 = BazaPodataka.UpdateOrDeleteQuery($"UPDATE [{np.tabela}] SET [stanje] = [stanje] + @kolicina WHERE [id] = @id", new Dictionary<string, object>
                        {
                            { "@id", np.id_u_tabeli },
                            { "@kolicina", kolicina },
                        }) > 0;
                        return uspesno2;
                    }
                }
                else
                {
                    throw new ArgumentException("Proizvod u datoj porudzbini nije narucen.");
                }
            }
            return false;
        }

        /*public static bool PrijemProizvoda(string kategorija, int id)
        {
            NabavniProizvod np = NabavniProizvod.Nadji(kategorija, id);
            if(np != null)
            {
                SqlDataReader reader = BazaPodataka.SelectQuery("SELECT [kolicina] FROM [Porudzbina] WHERE [id_proizvoda] = @id", new Dictionary<string, object>() { {"@id", np.id } });
                if(reader.Read())
                {
                    int kolicina = Convert.ToInt32(reader["kolicina"]);
                    if(kolicina <= 0)
                    {
                        throw new ArgumentException("Ovaj proizvod nije narucen.");
                    }
                    // Povecaj stanje
                    SqlDataReader readerStanje = BazaPodataka.SelectQuery($"SELECT [stanje] FROM {kategorija} WHERE [id] = @id", new Dictionary<string, object>() { { "@id", np.id_u_tabeli } });
                    if (readerStanje.Read())
                    {
                        int stanje = Convert.ToInt32(readerStanje["stanje"]);
                        bool updatedProizvod = BazaPodataka.UpdateOrDeleteQuery($"UPDATE {kategorija} SET [stanje] = @novo_stanje WHERE [id] = @id", new Dictionary<string, object>() {
                            { "@id", np.id_u_tabeli },
                            { "@novo_stanje",  stanje + kolicina}
                        }) > 0;
                        if(updatedProizvod)
                        {
                            // Postavi narucenu kolicinu na 0
                            bool updatedPorudzbina = BazaPodataka.UpdateOrDeleteQuery("UPDATE [Porudzbina] SET [kolicina] = 0 WHERE [id_proizvoda] = @id", new Dictionary<string, object>() { {"@id", np.id } }) > 0;
                            return updatedPorudzbina;
                        }
                    }
                }
            }
            return false;
        }*/

        public static int NadjiIdKategorije(string kategorija)
        {
            ValidacijaKategorije(kategorija);
            SqlDataReader reader = BazaPodataka.SelectQuery("SELECT [Id] from [Kategorija] WHERE [naziv] = @naziv", new Dictionary<string, object>() { { "@naziv", kategorija } });
            if(reader.Read())
            {
                return Convert.ToInt32(reader["Id"].ToString());
            }
            else
            {
                throw new Exception("Kategorija je prosla validaciju ali SqlDataReader nista ne cita iz rezultata.");
            }
        }

        public static int NadjiIdPoSifri(string kategorija, int sifra)
        {
            string strval = BazaPodataka.SelectSingleValueFromQuery($"SELECT [id] FROM [{kategorija}] WHERE [sifra] = @sifra", new Dictionary<string, object>() { { "@sifra", sifra } });
            return Convert.ToInt32(strval);
        }

        public static SqlDataReader SelectUpitNadSvimProizvodima(string upitNadJednomTabelom, Dictionary<string, object> parameters = null, string tabelaString = "__tabela__")
        {
            string query = "";
            for (int i = 0; i < Proizvodi.Kategorije.Count; i++)
            {
                string kategorija = Proizvodi.Kategorije[i];
                query += upitNadJednomTabelom.Replace(tabelaString, kategorija);
                if (i < Proizvodi.Kategorije.Count - 1)
                    query += " UNION ";
            }
            return BazaPodataka.SelectQuery(query, parameters);
        }
    }
}
