﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prodavnica
{
    public static class Statistika
    {
        public static SqlDataReader NadjiSlaboProdavaneProizvode()
        {
            string pojedinacanUpit =
                "SELECT [NabavniProizvod].[Id] as Id, '__tabela__' as kategorija, [sifra], [proizvodjac], [ime], [cena], [stanje], [garancija], [datum_poslednje_kupovine], [broj_prodatih]" + " " +
                "FROM [NabavniProizvod], [__tabela__]" + " " +
                "WHERE [tabela] = '__tabela__' AND" + " " +
                    "[id_u_tabeli] = [__tabela__].[id] AND" + " " +
                    "[datum_poslednje_kupovine] < GETDATE() - 30";
            return Proizvodi.SelectUpitNadSvimProizvodima(pojedinacanUpit);
        }

        public static List<string> NadjiKategorijeSlaboProdavanihProizvoda()
        {
            List<string> kategorije = new List<string>();
            string pojedinacanUpit =
                "SELECT  DISTINCT '__tabela__' as kategorija" + " " +
                "FROM [NabavniProizvod], [__tabela__]" + " " +
                "WHERE [tabela] = '__tabela__' AND" + " " +
                    "[id_u_tabeli] = [__tabela__].[id] AND" + " " +
                    "[datum_poslednje_kupovine] < GETDATE() - 30";
            SqlDataReader reader = Proizvodi.SelectUpitNadSvimProizvodima(pojedinacanUpit);
            while(reader.Read())
            {
                kategorije.Add(reader["kategorija"].ToString());
            }
            return kategorije;
        }
    }
}
