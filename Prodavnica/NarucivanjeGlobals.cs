﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prodavnica
{
    public static class NarucivanjeGlobals
    {
        public static PretragaControl pretragaControl = null;
        public static NarucivanjeControl narucivanjeControl = null;
        public static PrijemControl prijemControl = null;
        public static SlabaProdajaControl slabaProdajaControl = null;
    }
}
