﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prodavnica
{
    public static class KorisnickoUputstvo
    {
        private static string dodajKorisnika = @"
Kako dodati <strong>korisnika</strong>:
<ul>
    <li>Kliknite na karticu ""Korisnici"" </li>
    <li>Zatim kliknite na karticu ""Dodaj korisnika"" </li>
    <li>Popunite odgovarajuca polja</li>
    <li>Kliknite na dugme ""Dodaj""</li>
</ul>
";
        private static string izmeniKorisnika = @"
Kako izmeniti profil <strong>korisnika</strong>:
<ul>
    <li>Kliknite na karticu ""Korisnici"" </li>
    <li>Zatim kliknite na karticu ""Izmeni korisnika"" </li>
    <li>Popunite odgovarajuca polja</li>
    <li>Kliknite na dugme ""Izmeni""</li>
</ul>
";
        private static string izbrisiKorisnika = @"
Kako izbrisati profil <strong>korisnika</strong>:
<ul>
    <li>Kliknite na karticu ""Korisnici"" </li>
    <li>Zatim kliknite na karticu ""Izbrisi korisnika"" </li>
    <li>Kliknite na dugme ""Izbrisi"" kod korisnika kojeg zelite da izbrisete</li>
</ul>
";
        private static string dodavanjeProizvoda = @"
Kako dodati <strong>proizvode</strong>:
<ul>
    <li>Kliknite na karticu ""Proizvodi""</li>
    <li>Kliknite na karticu ""Dodaj (ime proizvoda koji zelite da dodate)""</li>
    <li>Popunite odgovarajuca polja</li>
    <li>Izaberite sliku</li>
    <li>Kliknite na ""Dodaj (ime proizvoda koji zelite da dodate)""</li>
</ul>
";
        private static string brisanjeProizvoda = @"
Kako izbrisati <strong>proizvode</strong>:
<ul>
    <li>Kliknite na karticu ""Proizvodi""</li>
    <li>Kliknite na karticu ""Obrisi/izmeni proizvod""</li>
    <li>Iz padajuceg menija klikom izaberite kategoriju proizvoda koji zelite da izbrisete</li>
    <li>U listi proizvoda pronadjite proizvod koji zelite da izbrisete i kliknite na dugme ""Izbrisi""</li>
</ul>
";
        private static string izmenaProizvoda = @"
Kako izmeniti <strong>proizvode</strong>:
<ul>
    <li>Kliknite na karticu ""Proizvodi""</li>
    <li>Kliknite na karticu ""Obrisi/izmeni proizvod""</li>
    <li>Iz padajuceg menija klikom izaberite kategoriju proizvoda koji zelite da izmnenite</li>
    <li>Izaberite proizvod koji zelite da izmenite</li>
    <li>Nakon zavrsenih izmena kliknite na dugme ""Izmeni""</li>
</ul>
";
        private static string promenaceneProizvoda = @"
Kako izmeniti cenu <strong>proizvode</strong>:
<ul>
    <li>Kliknite na karticu ""Proizvodi""</li>
    <li>Kliknite na karticu ""Promena cene""</li>
    <li>Unesite trazene informacije proizvoda</li>
    <li>Izaberite snizenje ili poskupljenje i unesite procenat snizenja/poskupljenja</li>
    <li>Kliknite na dugme ""Promeni cenu""</li>
</ul>
";
        private static string zamenaProizvoda = @"
Kako zameniti <strong>proizvode</strong>:
<ul>
    <li>Kliknite na karticu ""Zamena proizvoda""</li>
    <li>Pronadjite proizvod u listi kupljenih proizvoda koji zelite da zamenite</li>
    <li>U koliko nema trazenog proizvoda, kliknite na dugme ""Osvezi""</li>
    <li>Ukucajte u praznom polju broj racuna(id) i proverite da li je racun vazeci</li>
    <li>Kliknite na dugme ""Pretrazi""</li>
    <li>Iz nove liste izaberite novi proizvod kojim menjate stari i kliknite ""Dodaj u korpu""</li>
</ul>
";
        private static string neprodavaniProizvodi = @"
Kako manipulisati <strong>neprodavanim proizvodima</strong>:
<ul>
    <li>Kliknite na karticu ""Neprodavani proizvodi""</li>
    <li>U koliko zelite da smanjite cenu neprodavanog proizvoda, u listi izaberite neprodavani proizvod kome zelite da smanjite cenu i kliknite na dugme ""Potvrdi""</li>
    <li>U koliko zelite da cena neprodavanog proizvoda ostane ista, u listi izaberite neprodavani proizvod kome zelite da cena ostane ista i kliknite na dugme ""Otkazi""</li>
</ul>
";
        private static string narucenaRoba = @"
Kako manipulisati <strong>narucenom robom</strong>:
<ul>
    <li>Kliknite na karticu ""Narucena roba""</li>
    <li>Izaberite kategoriju narucene robe iz padajuceg menija</li>
    <li>Pojavice vam se liste gde mozete videti detaljan opis narucenog predmeta klikom na dugme ""Detaljniji opis""</li>
    <li>Izaberite dobavljaca iz padajuceg menija</li>
    <li>Mozete videti fakture klikom na dugme ""Prikazi fakture""</li>
    <li>Kliknite na dugme ""Prijem"" da biste narucili robu od izabranog dobavljaca</li>
</ul>
";
        private static string dobavljaci = @"
Kako manipulisati <strong>dobavljacima</strong>:
<ul>
    <li>Kliknite na karticu ""Dobavljaci""</li>
    <li>Kliknite na dugme ""Kategorija"" pored dobavljaca da promenite proizvode koji oni dobavljaju</li>
    <li>Nakon sto stiklirate proizvode koje zelite da dobavljac dobavlja, kliknite na dugme ""Sacuvaj"" ili na dugme ""Ponisti"" ako ne zelite da sacuvate izmene</li>
    <li>Da bi ste dodali dobavljaca, kliknite na dugme ""Dodaj"" nakon cega ce vam se otvoriti novi prozor u kome mozete uneti ime i email dobavljaca</li>
    <li>U koliko zelite da izaberete kategorijuproizvoda koju dobavljac dobavlja, kliknite na dugme ""Dalje >""</li>
    <li>U koliko zelite da odmah sacuvate bez dodavanja vrsta proizvoda koje dobavljac dobavlja, kliknite na dugme ""Kraj""</li>
    <li>U koliko zelite da ponistite unete informacije o dobaljvacu kliknite na dugme ""Ponisti""</li>
    <li>U koliko zelite da izbrisete dobavljaca, izaberite dobavljaca koga zelite da izbrisete sa liste i kliknite na dugme ""Izbrisi""</li>
</ul>
";
        private static string pretraga = @"
Kako pretraziti <strong>proizvode</strong>:
<ul>
    <li>Kliknite na karticu ""Pretraga""</li>
    <li>U padajucem meniju pronadjite zeljenu kategoriju proizvoda i kliknite na nju</li>
    <liProizvode mozete pretraziti i ukucavanjem imena proizvoda ili proizvodjaca</li>
    <li>Nakon sto ukucate ime proizvoda ili proizvodjaca kliknite na dugme ""Pretrazi""</li>
    <li>U listi izabrane vrste proizvoda mozete kliknuti na dugme ""Detaljniji opis"" da bi ste videli detaljan opis proizvoda</li>
    <li>Da bi ste stavili proizvod u korpu, kliknite na dugme ""Ubaci u korpu"" pored proizvoda koji zelite da ubacite u korpu</li>
</ul>
";
        private static string korpa = @"
Kako manipulisati <strong>korpom i kupovinom</strong>:
<ul>
    <li>Kliknite na karticu ""Korpa""</li>
    <li>U koliko zelite da izbacite proizvod iz korpe kliknite na dugme ""Izbaci iz korpe"" pored proizvoda kojeg zelite da izbacite iz korpe</li>
    <li>U koliko zelite da potvrdite kupovinu, kliknite na dugme ""Potvrdi kupovinu""</li>
</ul>
";
        private static string narucivanje = @"
Kako <strong>naruciti</strong> proizvode:
<ul>
    <li>Kliknite na karticu ""Narucivanje""</li>
    <li>Iz padajuceg menija izaberite kategoriju proizvoda koji zelite da narucite</li>
    <li>Iz liste izaberite proizvod koji zelite da narucite, unesite kolicinu tog proizvoda koji zelite da narucite i kliknite dugme ""Naruci"" da bi ste narucili izabrani proizvod</li>
</ul>
";
        private static string statsProizvoda = @"
Kako videti <strong>statistike slabo prodavanih proizvoda</strong>:
<ul>
    <li>Kliknite na karticu ""Statistike slabo prodavanih proizvoda""</li>
    <li>Iz padajuceg menija izaberite kategoriju proizvoda ciju statistiku zelite da vidite</li>
</ul>
";







        // Ovaj deo koda mora da ide ispod stringova stranica da bi radio
        public static ReadOnlyCollection<HelpStranica> HelpStranice { get; private set; } = new ReadOnlyCollection<HelpStranica>(new List<HelpStranica>
        {
            new HelpStranica("Dodavanje korisnika", dodajKorisnika, "A"),
            new HelpStranica("Menjanje profila korisnika", izmeniKorisnika, "A"),
            new HelpStranica("Brisanje profila korisnika", izbrisiKorisnika, "A"),
            new HelpStranica("Dodavanje proizvoda", dodavanjeProizvoda, "A"),
            new HelpStranica("Brisanje proizvoda", brisanjeProizvoda, "A"),
            new HelpStranica("Izmena proizvoda", izmenaProizvoda, "A"),
            new HelpStranica("Izmena cene proizvoda", promenaceneProizvoda, "A"),
            new HelpStranica("Neprodavani proizvodi", neprodavaniProizvodi, "A"),
            new HelpStranica("Narucena roba", narucenaRoba, "K"),
            new HelpStranica("Dobavljaci", dobavljaci, "K"),
            new HelpStranica("Pretraga", pretraga, "R"),
            new HelpStranica("Zamena proizvoda", zamenaProizvoda, "R"),
            new HelpStranica("Korpa", korpa, "R"),
            new HelpStranica("Narucivanje", narucivanje, "R"),
            new HelpStranica("Statistike slabo prodavanih proizvoda", statsProizvoda, "R"),

        });

        public static ReadOnlyCollection<HelpStranica> VratiStranicePoPrivilegiji(string nivoPrivilegije)
        {
            return new ReadOnlyCollection<HelpStranica>(HelpStranice.Where(x => Korisnici.DaLiImaPrivilegiju(nivoPrivilegije, x.NivoPrivilegije)).ToList());
        }
    }
}
