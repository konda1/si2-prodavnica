﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for ZamenaControl.xaml
    /// </summary>
    public partial class ZamenaControl : UserControlBase
    {
        string[] proizvodi = { "Adapter", "EksterniDisk", "HardDisk", "Kabl", "Mikrofon", "Mis", "Monitor", "Podloga", "Projektor", "Skener", "Slusalice", "Stampac", "Tastatura", "USBMemorija", "Zvucnik" };
        DataTable tabela = new DataTable();
        private double ukupnaCena { get { return RadnikGlobals.ukupnaCena; } set { RadnikGlobals.ukupnaCena = value; } }
        DataTable tabelaRacun { get { return RadnikGlobals.tabelaRacun; } set { RadnikGlobals.tabelaRacun = value; } }
        string strCena;

        public ZamenaControl(string title = null) : base(title)
        {
            tabelaRacun = new DataTable("Račun");
            tabelaRacun.Columns.Add("Proizvod", typeof(String));
            tabelaRacun.Columns.Add("Šifra proizvoda", typeof(String));
            tabelaRacun.Columns.Add("Proizvođač", typeof(String));
            tabelaRacun.Columns.Add("Ime proizvoda", typeof(String));
            tabelaRacun.Columns.Add("Cena", typeof(String));

            tabela.Columns.Add("id", typeof(String));
            tabela.Columns.Add("Proizvođač", typeof(String));
            tabela.Columns.Add("Ime proizvoda", typeof(String));
            tabela.Columns.Add("Šifra proizvoda", typeof(String));
            tabela.Columns.Add("Cena", typeof(String));
            tabela.Columns.Add("Proizvod", typeof(String));
            InitializeComponent();
            PrikaziRacune();
        }

        private void PrikaziRacune()
        {
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[Racuni]", conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);

                DataTable dt = new DataTable("Racuni");
                sda.Fill(dt);
                command.Dispose();
                sda.Dispose();
                conn.Close();
                DataGridTabelaRacuna.ItemsSource = dt.DefaultView;
            }
        }

        private void UpdatePriceTable(String price)
        {
            tabela.Clear();
            for (int i = 0; i < proizvodi.Length; i++)
            {
                string id = BazaPodataka.ReadFromDB(String.Format("SELECT [id] FROM [dbo].[{0}] WHERE [cena]<'{1}'", proizvodi[i], price));
                for (int j = 0; j < id.Length; j += 2)
                {
                    DataRow red = tabela.NewRow();
                    red[0] = id[j];
                    red[1] = BazaPodataka.ReadFromDB(String.Format("SELECT [proizvodjac] FROM [dbo].[{0}] WHERE [id]='{1}'", proizvodi[i], id[j]));
                    red[2] = BazaPodataka.ReadFromDB(String.Format("SELECT [ime] FROM [dbo].[{0}] WHERE [id]='{1}'", proizvodi[i], id[j]));
                    red[3] = BazaPodataka.ReadFromDB(String.Format("SELECT [sifra] FROM [dbo].[{0}] WHERE [id]='{1}'", proizvodi[i], id[j]));
                    red[4] = BazaPodataka.ReadFromDB(String.Format("SELECT [cena] FROM [dbo].[{0}] WHERE [id]='{1}'", proizvodi[i], id[j]));
                    red[5] = proizvodi[i];
                    tabela.Rows.Add(red);
                }
            }
            tabela.AcceptChanges();
            DataGridTabelaProizvoda.ItemsSource = tabela.DefaultView;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string brRacuna = textBoxBrRacuna.Text;
            string vazeci = BazaPodataka.ReadFromDB(String.Format("SELECT [vazeci] FROM [dbo].[Racuni] WHERE [id]='{0}'", brRacuna));
            if (vazeci.Equals("NE"))
            {
                MessageBox.Show("Morate izabrati vazeci račun.");
            }
            else
            {
                strCena = BazaPodataka.ReadFromDB(String.Format("SELECT [cena] FROM [dbo].[Racuni] WHERE [id]='{0}'", brRacuna));
                UpdatePriceTable(strCena);
                BazaPodataka.UpdateDB(String.Format("UPDATE [dbo].[Racuni] SET [vazeci]='NE' WHERE [id]='{0}'", brRacuna));
            }

        }
        
        private void BtnKorpa(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridTabelaProizvoda.ItemsSource = null;
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                double cena = Math.Round((1.21212121 * Double.Parse(dataRowView["Cena"].ToString())), 2);
                DataRow racunRed = tabelaRacun.NewRow();
                
                racunRed[0] = dataRowView["Proizvod"].ToString();
                racunRed[1] = dataRowView["Šifra proizvoda"].ToString();
                racunRed[2] = dataRowView["Proizvođač"].ToString();
                racunRed[3] = dataRowView["Ime proizvoda"].ToString();
                racunRed[4] = cena.ToString();
                strCena = (Double.Parse(strCena) - cena).ToString();
                tabelaRacun.Rows.Add(racunRed);
                RadnikGlobals.DataGridRacun.ItemsSource = tabelaRacun.DefaultView;
                if(Double.Parse(strCena)>0) UpdatePriceTable(strCena);
                RadnikGlobals.ukupnaCena += cena;
                RadnikGlobals.textBox.Text = "Vaš račun: " + (Math.Round(RadnikGlobals.ukupnaCena, 2)).ToString();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void DataGridTabelaRacuna_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DataGridTabelaProizvoda_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DataGridNoviRacun_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            PrikaziRacune();
        }
    }
}
