﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for UlogovanStrana.xaml
    /// </summary>
    public partial class UlogovanStrana : Window
    {
        string tipKorisnika;
        //int nivoKorisnika;
        //string TipKorisnika
        //{
        //    get { return tipKorisnika; }
        //    set
        //    {
        //        tipKorisnika = value;
        //        switch(tipKorisnika)
        //        {
        //            case "R":
        //                nivoKorisnika = 0;
        //                break;
        //            case "K":
        //                nivoKorisnika = 1;
        //                break;
        //            case "A":
        //                nivoKorisnika = 2;
        //                break;
        //            case "V":
        //                nivoKorisnika = 3;
        //                break;
        //            default:
        //                nivoKorisnika = -1;
        //                break;
        //        }
        //    }
        //}
        Dictionary<string, List<UserControlBase>> tabovi = new Dictionary<string, List<UserControlBase>> {
            { "V", new List<UserControlBase>{ } },
            { "A", new List<UserControlBase>{ new KorisniciControl(), new ProizvodiControl(), new NeprodavaniProizvodiControl("Neprodavani\nproizvodi") } },
            { "K", new List<UserControlBase>{ new PrijemControl("Naručena roba"), new DobavljaciControl("Dobavljači") } },
            { "R", new List<UserControlBase>{ new PretragaControl(), new ZamenaControl("Zamena\nproizvoda"), new KorpaControl(), new NarucivanjeControl("Naručivanje"), new SlabaProdajaControl("Statistike slabo\nprodavanih proizvoda") } },
        };
        public UlogovanStrana(string tipKorisnika)
        {
            InitializeComponent();
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);

            this.tipKorisnika = tipKorisnika;
            int pozicija = Korisnici.RedosledTipova.IndexOf(tipKorisnika);
            if (pozicija > -1)
            {
                for(int i = pozicija; i < Korisnici.RedosledTipova.Count; i++)
                {
                    string tip = Korisnici.RedosledTipova[i];
                    foreach (UserControlBase uc in tabovi[tip])
                    {
                        string tabName = uc.Title;
                        if (string.IsNullOrEmpty(tabName))
                        {
                            string className = uc.GetType().Name;
                            int indexOf = className.IndexOf("Control");
                            tabName = className.Substring(0, (indexOf < 0 ? 0 : indexOf));
                            if (string.IsNullOrEmpty(tabName))
                            {
                                tabName = className;
                            }
                        }

                        TabItem ti = new TabItem
                        {
                            Header = tabName,
                            Content = uc
                        };
                        tabControl.Items.Add(ti);
                    }
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            MainWindow mw = new MainWindow();
            mw.Show();
        }

        private void ButtonHelp_Click(object sender, RoutedEventArgs e)
        {
            new Help(tipKorisnika).Show();
        }
    }
}
