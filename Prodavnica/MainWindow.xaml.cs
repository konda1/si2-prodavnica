﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string username, password;

        public MainWindow()
        {
            InitializeComponent();
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
            FocusManager.SetFocusedElement(this, textBox);
        }

        private void registrujSe(object sender, RoutedEventArgs e)
        {
            /*string query = "INSERT INTO User VALUES(1, 'U', 'samar', 'samar', 'Nikola', Samardzic')";
            SqlCommand cmd = new SqlCommand(query);
           
            Registracija_strana rs = new Registracija_strana();
            this.Hide();
            rs.Show();*/
            string query = "SELECT Id FROM [dbo].[User] WHERE [ime]='administrator'";
            MessageBox.Show(BazaPodataka.ReadFromDB(query));
        }

        private void ulogujSe(object sender, RoutedEventArgs e)
        {
            username = textBox.Text;
            password = passwordBox.Password;

            

            string query = String.Format("SELECT tip_korisnika FROM [dbo].[User] WHERE [username]='{0}' AND [password]='{1}'", username, Admin_strana.MD5Hash(password));
            string tipKorisnika = BazaPodataka.ReadFromDB(query).Trim();
            List<string> tipovi = new List<string> { "V", "A", "K", "R" };
            if(tipovi.Contains(tipKorisnika))
            {
                UlogovanStrana glavnaStrana = new UlogovanStrana(tipKorisnika);
                Hide();
                glavnaStrana.Show();
                
                if(!tipKorisnika.Equals("R"))
                {
                    NeprodavaniProizvodiControl np = new NeprodavaniProizvodiControl(1);
                } 
            }
            else MessageBox.Show("Uneli ste pogrešnu lozinku i(ili) korisničko ime.");
            //if (Equals(tipKorisnika.Trim(), "A"))
            //{
            //    /*this.Hide();
            //    Admin_strana adm = new Admin_strana();
            //    adm.Show();*/
            //    this.Hide();
            //    Admin_strana adm = new Admin_strana();
            //    adm.Show();
            //}
            //else if (Equals(tipKorisnika.Trim(), "K"))
            //{
            //    this.Hide();
            //    Komercijalista_strana ks = new Komercijalista_strana();
            //    ks.Show();
            //}
            //else if (Equals(tipKorisnika.Trim(), "R"))
            //{
            //    this.Hide();
            //    Radnik_strana rs = new Radnik_strana();
            //    rs.Show();
            //}
            //else MessageBox.Show("Uneli ste pogrešnu lozinku i(ili) korisničko ime.");

        }
    }
}
