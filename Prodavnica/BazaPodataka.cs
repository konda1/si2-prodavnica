﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.Common;

namespace Prodavnica
{
    class BazaPodataka
    {
        public static string connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=ProdavnicaSI2;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        
        public static void InsertionIntoDB(string query)
        {
            
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
               conn.Open();
               SqlDataAdapter sda = new SqlDataAdapter();
               try
                {                   
                    sda.InsertCommand = new SqlCommand(query, conn);
                    sda.InsertCommand.ExecuteNonQuery();
                    MessageBox.Show("Uspesno");
                }
                catch(Exception e)
                {
                    MessageBox.Show("Neuspesno");
                    Console.WriteLine(e);
                }
            }
        }

        public static string ReadFromDB(string query)
        {
            string data = "";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                try
                {
                    SqlCommand command = new SqlCommand(query, conn);
                    SqlDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        data += dr[0].ToString();
                        data += " ";
                    }
                    data = data.Trim();

                    //dr.FieldCount();
                    dr.Close();
                    return data;
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return data;
                }

            }
           
        }

        public static void KreiranjeTabele(string ime, params string[] kolone)
        {
            StringBuilder command = new StringBuilder();
            command.Append("CREATE TABLE [dbo].[" + ime + "] (ID INT IDENTITY(1,1) NOT NULL PRIMARY KEY, ");
            foreach(string kolona in kolone)
            {
                command.Append("[" + kolona + "] VARCHAR(50), ");
            }
            command.Remove(command.Length - 2, 2);
            command.Append(")");

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter();
                try
                {
                    sda.InsertCommand = new SqlCommand(command.ToString(), conn);
                    sda.InsertCommand.ExecuteNonQuery();
                    MessageBox.Show("Uspesno");
                }
                catch (Exception e)
                {
                    MessageBox.Show("Neuspesno");
                    Console.WriteLine(e);
                }
            }
        }

        public static bool ProveraSifre(string tabela, int sifra)
        {
            string query = String.Format("SELECT ime FROM {0} WHERE [sifra] = {1}", tabela, sifra);
            string korisnik = BazaPodataka.ReadFromDB(query);
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter();
                try
                {
                    sda.InsertCommand = new SqlCommand(query, conn);
                    sda.InsertCommand.ExecuteNonQuery();
                    if (!korisnik.Equals(""))
                        return false;
                    else
                        return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            }
        }

        public static bool UpdateDB(string query)
        {

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter();
                try
                {
                    sda.UpdateCommand = new SqlCommand(query, conn);
                    sda.UpdateCommand.ExecuteNonQuery();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            }
        }

        public static bool DeleteFromDB(string query)
        {

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter();
                try
                {
                    sda.DeleteCommand = new SqlCommand(query, conn);
                    sda.DeleteCommand.ExecuteNonQuery();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            }
        }

        public static SqlDataReader ExecuteReader(string connectionString, string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            SqlConnection conn = new SqlConnection(connectionString);

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.AddRange(parameters);

                conn.Open();
                // When using CommandBehavior.CloseConnection, the connection will be closed when the 
                // IDataReader is closed.
                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                return reader;
            }
        }

        public static SqlParameter[] ParameterDictionaryToSqlParameterArray(Dictionary<string, object> parameters)
        {
            SqlParameter[] sqlParameters = new SqlParameter[parameters.Count];
            int index = 0;
            foreach (KeyValuePair<string, object> pair in parameters)
            {
                sqlParameters[index++] = new SqlParameter(pair.Key, pair.Value);
            }
            return sqlParameters;
        }

        public static SqlDataReader ExecuteReader(string connectionString, string commandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            SqlParameter[] sqlParameters = ParameterDictionaryToSqlParameterArray(parameters);
            return ExecuteReader(connectionString, commandText, commandType, sqlParameters);
        }

        public static int ExecuteNonQuery(string connectionString, string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(commandText, conn))
                {
                    cmd.CommandType = commandType;
                    cmd.Parameters.AddRange(parameters);

                    int rows = cmd.ExecuteNonQuery();
                    return rows;
                }
            }
        }

        public static int ExecuteNonQuery(string connectionString, string commandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            SqlParameter[] sqlParameters = ParameterDictionaryToSqlParameterArray(parameters);
            return ExecuteNonQuery(connectionString, commandText, commandType, sqlParameters);
        }

        public static int CountQuery(string query, string count_name)
        {
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    return Convert.ToInt32(reader[count_name].ToString());
                }
            }
        }

        public static SqlDataReader SelectQuery(string query, Dictionary<string, object> parameters = null)
        {
            parameters = parameters ?? new Dictionary<string, object>();
            return ExecuteReader(BazaPodataka.connectionString, query, CommandType.Text, parameters);
        }

        public static int UpdateOrDeleteQuery(string query, Dictionary<string, object> parameters = null)
        {
            parameters = parameters ?? new Dictionary<string, object>();
            return ExecuteNonQuery(BazaPodataka.connectionString, query, CommandType.Text, parameters);
        }

        public static string SelectSingleValueFromQuery(string query, Dictionary<string, object> parameters = null)
        {
            SqlDataReader reader = SelectQuery(query, parameters);
            if(reader.Read())
            {
                return reader[0].ToString();
            }
            else
            {
                return null;
            }
        }

        public static void InsertionIntoDBWithoutMessageBox(string query)
        {

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter();
                try
                {
                    sda.InsertCommand = new SqlCommand(query, conn);
                    sda.InsertCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}
