﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for IzmenaKategorija.xaml
    /// </summary>
    public partial class IzmenaKategorija : Window
    {
        private int id_dobavljaca;
        List<Kategorija> lista_kategorija;
        public IzmenaKategorija(int id_dobavljaca)
        {
            InitializeComponent();
            this.id_dobavljaca = id_dobavljaca;
            RefreshDataGrid();
        }

        class Kategorija
        {
            public string Naziv { get; set; }
            public bool Ukljucena { get; set; }

            public static List<Kategorija> NapraviListu(List<string> ukljucene_kategorije)
            {
                List<Kategorija> kategorije = new List<Kategorija>();
                foreach (string k in Proizvodi.Kategorije)
                {
                    kategorije.Add(new Kategorija {
                        Naziv = k,
                        Ukljucena = Utils.StringInStringList(k, ukljucene_kategorije)
                    });
                }
                return kategorije;
            }

            public static List<string> UkljuceneKategorije(List<Kategorija> lista)
            {
                List<string> kategorije = new List<string>();
                foreach (Kategorija k in lista)
                    if (k.Ukljucena)
                        kategorije.Add(k.Naziv);
                return kategorije;
            }
        }

        public void RefreshDataGrid()
        {
            List<string> kategorije = Dobavljaci.NadjiKategorijeDobavljaca(id_dobavljaca);
            dataGridKategorije.ItemsSource = lista_kategorija = Kategorija.NapraviListu(kategorije);
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Dobavljaci.PostaviKategorijeZaDobavljaca(id_dobavljaca, Kategorija.UkljuceneKategorije(lista_kategorija));
                Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Neuspesno");
            }
        }

        private void Output_Click(object sender, RoutedEventArgs e)
        {
            int num_checked = 0;
            foreach (Kategorija k in dataGridKategorije.ItemsSource)
                num_checked += (k.Ukljucena ? 1 : 0);
            Console.WriteLine(num_checked);
        }

        private void Ponisti_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
