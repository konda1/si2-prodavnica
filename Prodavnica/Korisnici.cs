﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prodavnica
{
    public static class Korisnici
    {
        public static ReadOnlyCollection<string> RedosledTipova { get; private set; } = new ReadOnlyCollection<string>(new List<string> { "V", "A", "K", "R" });

        public static bool ValidacijaPrivilegije(string nivoPrivilegije)
        {
            return Utils.StringInStringList(nivoPrivilegije, RedosledTipova.ToList());
        }

        public static void EValidacijaPrivilegije(string nivoPrivilegije)
        {
            if(!ValidacijaPrivilegije(nivoPrivilegije))
            {
                throw new ArgumentOutOfRangeException("nivoPrivilegije", nivoPrivilegije, "Dozvoljene vrednosti za privilegije korisnika su: \"V\", \"A\", \"K\", \"R\"");
            }
        }

        public static bool DaLiImaPrivilegiju(string nivoKorisnika, string nivoPrivilegije)
        {
            EValidacijaPrivilegije(nivoKorisnika);
            EValidacijaPrivilegije(nivoPrivilegije);

            int indexKorisnik = RedosledTipova.IndexOf(nivoKorisnika);
            int indexPrivilegija = RedosledTipova.IndexOf(nivoPrivilegije);
            if (indexKorisnik == -1 || indexPrivilegija == -1)
                return false;
            return indexKorisnik <= indexPrivilegija;
        }
    }
}
