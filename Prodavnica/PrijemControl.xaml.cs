﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for PrijemControl.xaml
    /// </summary>
    public partial class PrijemControl : UserControlBase
    {
        string tablename;
        public int SelectedIdPorudzbine { get; set; }
        public int SelectedIndexDobavljaca { get; set; }
        List<Dobavljac> dobavljaci;
        public PrijemControl(string title = null) : base(title)
        {
            InitializeComponent();
            NarucivanjeGlobals.prijemControl = this;
        }

        public void RefreshDataGrid()
        {
            if(tablename == null || ComboBox.SelectedItem == null || !Utils.StringInStringList(((ComboBoxItem)ComboBox.SelectedItem).Content.ToString(), Proizvodi.NaruceneKategorije()))
            {
                ComboBox.SelectedItem = null;
                DataGrid.ItemsSource = null;
                comboBoxDobavljac.IsEnabled = false;
                labelDobavljac.IsEnabled = false;
                btnPrijem.IsEnabled = false;
                ResetComboBoxDobavljac();
            }
            else
            {
                DataTable dt = Proizvodi.NadjiNaruceneProizvode(tablename);
                DataGrid.ItemsSource = dt.DefaultView;
            }
        }

        public void ResetComboBoxDobavljac()
        {
            comboBoxDobavljac.Items.Clear();
            if (comboBoxDobavljac.IsEnabled)
            {
                comboBoxDobavljac.Items.Add(new ComboBoxItem { Content = "Izaberite dobavljača...", Name = "initial" });
                comboBoxDobavljac.SelectedIndex = 0;
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0) // Caused by Clear()
            {
                return;
            }
            ComboBoxItem item = (ComboBoxItem)ComboBox.SelectedItem;
            tablename = item.Content.ToString();

            dobavljaci = Dobavljaci.NadjiDobavljaceKategorije(Proizvodi.NadjiIdKategorije(tablename));

            labelDobavljac.IsEnabled = true;
            comboBoxDobavljac.IsEnabled = true;
            btnPrijem.IsEnabled = true;
            ResetComboBoxDobavljac();

            try
            {
                RefreshDataGrid();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Neuspesno");
                Console.WriteLine(ex);
            }
        }

        private void BtnPrijem_Click(object sender, RoutedEventArgs e)
        {
            string fakturaPath = null;
            try
            {
                if (comboBoxDobavljac.SelectedItem == null || "initial".CompareTo(((ComboBoxItem)comboBoxDobavljac.SelectedItem).Name) == 0)
                {
                    MessageBox.Show("Izaberite dobavljaca.");
                    return;
                }
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;

                fakturaPath = Fakture.NapraviFakturu(SelectedIdPorudzbine, dobavljaci[SelectedIndexDobavljaca].Id);
                bool uspesno = Proizvodi.PrijemProizvoda(SelectedIdPorudzbine);
                if (uspesno)
                {
                    RefreshDataGrid();
                    NarucivanjeGlobals.narucivanjeControl.RefreshDataGrid();
                    NarucivanjeGlobals.pretragaControl.RefreshDataGrid();
                }
                else
                {
                    if (fakturaPath != null)
                        File.Delete(fakturaPath);
                }
                MessageBox.Show(uspesno ? "Uspesno" : "Neuspesno");
            }
            catch(ArgumentException ex)
            {
                if (fakturaPath != null)
                    File.Delete(fakturaPath);
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                if (fakturaPath != null)
                    File.Delete(fakturaPath);
                MessageBox.Show("Neuspesno");
                Console.WriteLine(ex);
            }

        }

        private void BtnOpis(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                OpisProizvoda op = new OpisProizvoda(dataRowView["id"].ToString(), tablename);
                op.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }

        private void ComboBox_DropDownOpened(object sender, EventArgs e)
        {
            ComboBox.Items.Clear();
            foreach (string kategorija in Proizvodi.NaruceneKategorije())
            {
                ComboBox.Items.Add(new ComboBoxItem
                {
                    Content = kategorija
                });
            }
        }

        private void ComboBoxDobavljac_DropDownOpened(object sender, EventArgs e)
        {
            comboBoxDobavljac.Items.Clear();
            foreach (Dobavljac dobavljac in dobavljaci)
            {
                comboBoxDobavljac.Items.Add(new ComboBoxItem
                {
                    Content = $"{dobavljac.Naziv} ({dobavljac.Email})",
                });
            }
        }

        private void FakturaTest_Click(object sender, RoutedEventArgs e)
        {
            Fakture.NapraviFakturu(SelectedIdPorudzbine, dobavljaci[SelectedIndexDobavljaca].Id);
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView item = DataGrid.SelectedItem as DataRowView;
            if (item != null)
                SelectedIdPorudzbine = Convert.ToInt32(item.Row["id_porudzbine"].ToString());
        }

        private void ComboBoxDobavljac_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedIndexDobavljaca = comboBoxDobavljac.SelectedIndex;
        }

        private void Fakture_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(@"..\..\Fakture\");
        }
    }
}
