﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for KorisniciControl.xaml
    /// </summary>
    public partial class KorisniciControl : UserControlBase
    {
        public KorisniciControl(string title = null) : base(title)
        {
            InitializeComponent();
            UpdateUserTable();
        }

        private void UpdateUserTable()
        {
            string query = String.Format("SELECT [Id], [tip_korisnika], [username], [ime], [prezime] FROM [dbo].[User]");
            using (SqlConnection conn = new SqlConnection(BazaPodataka.connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                try
                {
                    DataTable dt = new DataTable("Korisnici");
                    sda.Fill(dt);
                    ///DataGrid.ItemsSource = dt.DefaultView;
                    sda.Update(dt);
                    command.Dispose();
                    sda.Dispose();
                    conn.Close();
                    DataGridUser.ItemsSource = dt.DefaultView;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Neuspesno");
                    Console.WriteLine(ex);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DataGridUser.ItemsSource = null;
            string hashPass = MD5Hash(pass.Password);
            BazaPodataka.InsertionIntoDB(String.Format("INSERT INTO [dbo].[User] ([tip_korisnika], [username], [password], [ime], [prezime]) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", tip.Text, user.Text, hashPass, ime.Text, prezime.Text));
            UpdateUserTable();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string hashPass = MD5Hash(izmeniPass.Text);
            if (izmeniPass.Text.Equals(""))
                BazaPodataka.InsertionIntoDB(String.Format("UPDATE [dbo].[User] SET [tip_korisnika] ='{0}' WHERE [username] = '{1}'", izmeniTip.Text, izmeniUser.Text));
            else if (izmeniTip.Text.Equals(""))
                BazaPodataka.InsertionIntoDB(String.Format("UPDATE [dbo].[User] SET [password] ='{0}' WHERE [username] = '{1}'", hashPass, izmeniUser.Text));
            else
                BazaPodataka.InsertionIntoDB(String.Format("UPDATE [dbo].[User] SET [password] ='{0}', [tip_korisnika] = '{2}' WHERE [username] = '{1}'", hashPass, izmeniUser.Text, izmeniTip.Text));

        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("x2"));
            }
            return sb.ToString();
        }

        private void BtnObrisi(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridUser.ItemsSource = null;
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                int id = Int32.Parse(dataRowView[0].ToString());
                String query = String.Format("DELETE FROM [dbo].[User] Where [Id] = {0}", id);
                BazaPodataka.InsertionIntoDB(query);
                UpdateUserTable();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }
    }
}
