﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prodavnica
{
    public static class Fakture
    {
        private class PorucenProizvod
        {
            public string Sifra { get; set; }
            public string Proizvodjac { get; set; }
            public string Ime { get; set; }
            public string Kolicina { get; set; }
            public string Cena { get; set; }

            public static PorucenProizvod Nadji(int id_porudzbine)
            {
                string query_kategorija =
                    "SELECT [tabela]" + " " +
                    "FROM [Porudzbina] JOIN [NabavniProizvod]" + " " +
                    "ON [Porudzbina].[id_proizvoda] = [NabavniProizvod].[Id]" + " " +
                    "WHERE [Porudzbina].[id] = @id_porudzbine";
                string tabela = BazaPodataka.SelectSingleValueFromQuery(query_kategorija, new Dictionary<string, object>
                {
                    { "@id_porudzbine", id_porudzbine }
                });

                string query =
                    "SELECT [sifra], [proizvodjac], [ime], [kolicina], [cena]" + " " +
                    $"FROM [Porudzbina], [NabavniProizvod], [{tabela}]" + " " +
                    $"WHERE [{tabela}].[id] = [NabavniProizvod].[id_u_tabeli] AND" + " " +
                        "[NabavniProizvod].[Id] = [Porudzbina].[id_proizvoda] AND" + " " +
                        $"[NabavniProizvod].[tabela] = '{tabela}' AND" + " " +
                        "[Porudzbina].[Id] = @id_porudzbine";
                Dictionary<string, object> parameters = new Dictionary<string, object>
                {
                    { "@id_porudzbine", id_porudzbine }
                };
                SqlDataReader reader = BazaPodataka.SelectQuery(query, parameters);
                if(reader.Read())
                    return new PorucenProizvod
                    {
                        Sifra = reader["sifra"].ToString(),
                        Proizvodjac = reader["proizvodjac"].ToString(),
                        Ime = reader["ime"].ToString(),
                        Kolicina = reader["kolicina"].ToString(),
                        Cena = string.Format("{0:0.00}", reader["cena"]),
                    };
                else
                    return null;
            }
        }

        private static Document InicijalizujFakturu(out PdfWriter wri, out string path)
        {
            path = String.Format("../../Fakture/faktura-{0}.pdf", DateTime.Now.ToString("dd_MM_yyyy-hh_mm_ss"));
            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
            wri = PdfWriter.GetInstance(doc, new FileStream(@path, FileMode.Create));
            doc.Open();
            return doc;
        }

        private static void ZavrsiFakturu(Document doc, PdfWriter wri)
        {
            //wri.Close();
            doc.Close();
        }

        private static Document DodajInformacijeODobavljacu(Document doc, Dobavljac dobavljac)
        {
            doc.Add(new Paragraph($"Dobavljač: {dobavljac.Naziv}"));
            doc.Add(new Paragraph($"E-mail adresa: {dobavljac.Email}"));
            doc.Add(Chunk.NEWLINE);
            return doc;
        }

        private static Document DodajTabelu(Document doc, params List<string>[] redovi)
        {
            if (redovi.Length > 0)
            {
                PdfPTable table = new PdfPTable(redovi[0].Count);
                table.WidthPercentage = 100;

                bool prviRed = true;
                foreach(List<string> red in redovi)
                {
                    foreach (string vrednostPolja in red)
                    {
                        bool noBorder = vrednostPolja.StartsWith("__noborder__");
                        PdfPCell cell = new PdfPCell()
                        {
                            Phrase = new Phrase(vrednostPolja.Replace("__noborder__", "")),
                            HorizontalAlignment = PdfPCell.ALIGN_CENTER,
                            VerticalAlignment = PdfPCell.ALIGN_CENTER
                        };
                        if (prviRed)
                            cell.BackgroundColor = new iTextSharp.text.BaseColor(51, 102, 102);
                        if (noBorder)
                            cell.Border = Rectangle.NO_BORDER;

                        table.AddCell(cell);
                    }
                    if (prviRed)
                        prviRed = false;
                }

                doc.Add(table);
            }
            return doc;
        }

        private static List<string> SumiranRed(PorucenProizvod proizvod, int br_kolona)
        {
            List<string> red = new List<string>();

            for (int i = 0; i < br_kolona - 2; i++)
                red.Add("__noborder__");
            red.Add("__noborder__Ukupno:");
            red.Add(string.Format("{0:0.00}", float.Parse(proizvod.Cena) * Convert.ToInt32(proizvod.Kolicina)));

            return red;
        }

        private static Document DodajInformacijeOProizvodu(Document doc, PorucenProizvod proizvod)
        {
            Dictionary<string, string> kolone = new Dictionary<string, string>
            {
                { "Šifra proizvoda", proizvod.Sifra },
                { "Proizvođač", proizvod.Proizvodjac },
                { "Ime proizvoda", proizvod.Ime },
                { "Količina", proizvod.Kolicina },
                { "Cena 1x", proizvod.Cena },
            };

            DodajTabelu(doc, kolone.Keys.ToList(), kolone.Values.ToList(), SumiranRed(proizvod, kolone.Count));
            return doc;
        }

        public static string NapraviFakturu(int id_porudzbine, int id_dobavljaca)
        {
            PorucenProizvod proizvod = PorucenProizvod.Nadji(id_porudzbine);
            Dobavljac dobavljac = Dobavljac.Nadji(id_dobavljaca);

            string path;
            PdfWriter wri;
            Document doc = InicijalizujFakturu(out wri, out path);

            DodajInformacijeODobavljacu(doc, dobavljac);
            DodajInformacijeOProizvodu(doc, proizvod);

            ZavrsiFakturu(doc, wri);
            return path;
        }
    }
}
