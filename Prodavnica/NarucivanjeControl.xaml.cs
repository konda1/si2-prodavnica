﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prodavnica
{
    /// <summary>
    /// Interaction logic for NarucivanjeControl.xaml
    /// </summary>
    public partial class NarucivanjeControl : UserControlBase
    {
        string tablename = null;
        public NarucivanjeControl(string title = null) : base(title)
        {
            InitializeComponent();
            NarucivanjeGlobals.narucivanjeControl = this;
        }

        public void RefreshDataGrid()
        {
            if (tablename == null || ComboBox.SelectedItem == null || !Utils.StringInStringList(((ComboBoxItem)ComboBox.SelectedItem).Content.ToString(), Proizvodi.KategorijeBezStanja()))
            {
                ComboBox.SelectedItem = null;
                DataGrid.ItemsSource = null;
            }
            else
            {
                DataTable dt = Proizvodi.NadjiProizvodeBezStanja(tablename);
                DataGrid.ItemsSource = dt.DefaultView;
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0) // Caused by Clear()
            {
                return;
            }
            ComboBoxItem item = (ComboBoxItem)ComboBox.SelectedItem;
            tablename = item.Content.ToString();

            label.IsEnabled = true;
            textBox.IsEnabled = true;

            try
            {
                RefreshDataGrid();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Neuspesno");
                Console.WriteLine(ex);
            }
        }

        private void BtnNaruci(object sender, RoutedEventArgs e)
        {
            try
            {
                int kolicina = (textBox.Text.Length == 0 ? 0 : Convert.ToInt32(textBox.Text));
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;

                bool uspesno = Proizvodi.NaruciProizvod(tablename, Convert.ToInt32(dataRowView[0].ToString()), kolicina);
                if(uspesno)
                {
                    NarucivanjeGlobals.prijemControl?.RefreshDataGrid();
                }
                MessageBox.Show(uspesno ? "Uspesno" : "Neuspesno");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Neuspesno");
                Console.WriteLine(ex);
            }

        }

        private void BtnOpis(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                OpisProizvoda op = new OpisProizvoda(dataRowView["id"].ToString(), tablename);
                op.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex(@"[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ComboBox_DropDownOpened(object sender, EventArgs e)
        {
            ComboBox.Items.Clear();
            foreach(string kategorija in Proizvodi.KategorijeBezStanja())
            {
                ComboBox.Items.Add(new ComboBoxItem {
                    Content = kategorija
                });
            }
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
